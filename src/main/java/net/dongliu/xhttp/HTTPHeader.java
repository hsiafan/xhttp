package net.dongliu.xhttp;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;
import static java.util.Objects.requireNonNull;

/**
 * Http header
 */
public class HTTPHeader implements NameValue {
    private final String name;
    private final String value;

    private HTTPHeader(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Create new header, with date value
     */
    public static HTTPHeader ofDate(String name, Date date) {
        requireNonNull(name);
        requireNonNull(date);
        return ofDate(name, date.toInstant());
    }

    /**
     * Create new header, with datetime value
     */
    public static HTTPHeader ofDate(String name, ZonedDateTime dateTime) {
        requireNonNull(name);
        requireNonNull(dateTime);
        return new HTTPHeader(name, RFC_1123_DATE_TIME.format(dateTime.withZoneSameInstant(UTC)));
    }

    /**
     * Create new header, with datetime value
     */
    public static HTTPHeader ofDate(String name, OffsetDateTime dateTime) {
        requireNonNull(name);
        requireNonNull(dateTime);
        return new HTTPHeader(name, RFC_1123_DATE_TIME.format(dateTime.withOffsetSameInstant(UTC)));
    }

    /**
     * Create new header, with instant
     */
    public static HTTPHeader ofDate(String name, Instant instant) {
        requireNonNull(name);
        requireNonNull(instant);
        return new HTTPHeader(name, RFC_1123_DATE_TIME.format(instant.atOffset(UTC)));
    }

    /**
     * Create new header, with long value
     */
    public static HTTPHeader ofLong(String name, long value) {
        return new HTTPHeader(name, String.valueOf(value));
    }


    /**
     * Create new HTTPHeader.
     *
     * @param name  the header name
     * @param value the header value
     * @return the header
     */
    public static HTTPHeader of(String name, String value) {
        return new HTTPHeader(requireNonNull(name), requireNonNull(value));
    }

    @Override
    public boolean nameEquals(String name) {
        return name().equals(name);
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HTTPHeader that = (HTTPHeader) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, value);
    }

    @Override
    public String toString() {
        return name + ": " + value;
    }
}
