package net.dongliu.xhttp.body;

import net.dongliu.xhttp.MimeType;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

class Files2 {

    static Optional<MimeType> detectFileType(Path path) {
        try {
            String probeContentType = Files.probeContentType(path);
            return MimeType.safeParse(probeContentType);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
