package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.US_ASCII;

/**
 * MultiPart post body
 *
 * @author Liu Dong
 */
class MultiPartBody extends AbstractBody<List<Part>> {
    private static final String BOUNDARY = "********************" + System.currentTimeMillis();
    private static final ContentType contentType = ContentType.multiPart(BOUNDARY);
    private static final String LINE_END = "\r\n";

    MultiPartBody(List<Part> body) {
        super(body, contentType);
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        List<BodyPublisher> publishers = new ArrayList<>();
        for (Part part : body()) {
            publishers.add(BodyPublishers.ofString("--" + BOUNDARY + LINE_END, US_ASCII));

            String header = part.headerData();
            publishers.add(BodyPublishers.ofString(header, US_ASCII));

            publishers.add(part.asPublisher());

            publishers.add(BodyPublishers.ofByteArray(LINE_END.getBytes()));
        }
        publishers.add(BodyPublishers.ofString("--" + BOUNDARY + "--" + LINE_END, US_ASCII));
        return new AggregateBodyPublisher(publishers);
    }

}
