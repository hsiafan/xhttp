package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;

/**
 * This HTTPRequest Body which provided by InputStream.
 *
 * @author Liu Dong
 */
class InputStreamBody extends AbstractBody<InputSupplier> {

    InputStreamBody(InputSupplier body, ContentType contentType) {
        super(body, contentType);
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        return BodyPublishers.ofInputStream(body());
    }
}
