package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;
import net.dongliu.xhttp.MimeType;
import net.dongliu.xhttp.exception.JsonProcessorNotFoundException;
import net.dongliu.xhttp.json.JsonProcessor;
import net.dongliu.xhttp.json.ReactiveJsonProcessor;

import java.net.http.HttpRequest.BodyPublisher;
import java.nio.charset.Charset;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Http body carry json content.
 *
 * @author Liu Dong
 */
public class JsonBody<T> extends AbstractBody<T> {

    JsonBody(T body, Charset charset) {
        super(body, ContentType.of(MimeType.JSON, charset));
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        var jsonProcessor = ensureJsonProcessor(context.jsonProcessor());
        var charset = contentType().charset().orElse(UTF_8);
        return ReactiveJsonProcessor.delegate(jsonProcessor).asPublisher(body(), charset);
    }

    private JsonProcessor ensureJsonProcessor(JsonProcessor jsonProcessor) {
        if (jsonProcessor == null) {
            throw new JsonProcessorNotFoundException();
        }
        return jsonProcessor;
    }
    
}
