package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;

/**
 * HTTPRequest body contains binary content.
 *
 * @author Liu Dong
 */
class BinaryBody extends AbstractBody<byte[]> {

    BinaryBody(byte[] body, ContentType contentType) {
        super(body, contentType);
    }


    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        return BodyPublishers.ofByteArray(body());
    }
}
