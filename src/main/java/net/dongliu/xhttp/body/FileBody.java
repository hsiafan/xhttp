package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.nio.file.Path;

/**
 * HTTPRequest body contains a file
 *
 * @author Liu Dong
 */
class FileBody extends AbstractBody<Path> {

    FileBody(Path body, ContentType contentType) {
        super(body, contentType);
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        try {
            return BodyPublishers.ofFile(body());
        } catch (FileNotFoundException e) {
            throw new UncheckedIOException(e);
        }
    }
}
