package net.dongliu.xhttp.body;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.json.JsonProcessor;

/**
 * Context for publishing body
 */
public class BodyPublishContext {
    @Nullable
    private final JsonProcessor jsonProcessor;

    public BodyPublishContext(@Nullable JsonProcessor jsonProcessor) {
        this.jsonProcessor = jsonProcessor;
    }

    JsonProcessor jsonProcessor() {
        return jsonProcessor;
    }
}
