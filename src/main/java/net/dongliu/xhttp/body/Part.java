package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;
import net.dongliu.xhttp.HTTPHeaderNames;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

/**
 * One part of multipart request.
 *
 * @param <T> the content type
 * @author Liu Dong
 */
public abstract class Part<T> {
    // part name
    private final String name;
    private final T content;

    private static final String LINE_END = "\r\n";

    /**
     * @param name    the part name
     * @param content the content
     */
    protected Part(String name, T content) {
        this.name = requireNonNull(name);
        this.content = requireNonNull(content);
    }

    /**
     * The part name
     */
    public String name() {
        return name;
    }


    /**
     * The part content.
     */
    public T content() {
        return content;
    }

    /**
     * Get the content as InputStream. Implementations should return one new InputStream, for each call.
     *
     * @return The InputStream
     */
    protected abstract BodyPublisher asPublisher();

    /**
     * Get header of part.
     */
    protected abstract String headerData();

    /**
     * The ordinary Part, which has Content, Content-Type. This is usually for upload file, so call it FilePart
     */
    static class FilePart extends Part<InputSupplier> {

        /**
         * The filename
         */
        private final String filename;
        private ContentType contentType;

        /**
         * @param name     the part name
         * @param filename the filename
         * @param supplier supply InputStream
         */
        FilePart(String name, String filename, InputSupplier supplier, ContentType contentType) {
            super(name, supplier);
            this.filename = requireNonNull(filename);
            this.contentType = requireNonNull(contentType);
        }

        @Override
        protected String headerData() {
            StringBuilder sb = new StringBuilder("Content-Disposition: form-data; name=\"").append(name()).append("\"");
            sb.append("; filename=\"").append(filename).append('"');
            sb.append(LINE_END);
            sb.append(HTTPHeaderNames.CONTENT_TYPE).append(": ").append(contentType.toString()).append(LINE_END);
            sb.append(LINE_END);
            return sb.toString();
        }

        @Override
        protected BodyPublisher asPublisher() {
            return BodyPublishers.ofInputStream(content());
        }

        /**
         * The part content type. may be empty
         */
        public ContentType contentType() {
            return contentType;
        }
    }

    /**
     * The MultiPart form text field. This Part do not set ContentType.
     */
    static class TextPart extends Part<String> {

        /**
         * @param name the part name
         * @param text the text content
         */
        TextPart(String name, String text) {
            super(name, text);
        }


        @Override
        protected BodyPublisher asPublisher() {
            //TODO: just use utf-8 charset here?
            return BodyPublishers.ofInputStream(InputSuppliers.of(content(), UTF_8));
        }

        @Override
        protected String headerData() {
            StringBuilder sb = new StringBuilder("Content-Disposition: form-data; name=\"").append(name()).append("\"");
            sb.append(LINE_END);
            sb.append(LINE_END);
            return sb.toString();
        }
    }

}
