package net.dongliu.xhttp.body;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.Objects.requireNonNull;

/**
 * InputStream Supplier Utils
 */
public class InputSuppliers {

    /**
     * Create InputSupplier from File Path.
     *
     * @param path the file path
     * @return InputSupplier
     */
    public static InputSupplier of(Path path) {
        return () -> {
            try {
                return Files.newInputStream(path);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };
    }

    /**
     * Create InputSupplier from byte array data.
     *
     * @param bytes the data
     * @return InputSupplier
     */
    public static InputSupplier of(byte[] bytes) {
        return () -> new ByteArrayInputStream(bytes);
    }

    /**
     * Utils method to wrap string to InputStream Supplier.
     *
     * @param str the string
     * @return inputStream
     */
    static InputSupplier of(String str, Charset charset) {
        requireNonNull(str);
        requireNonNull(charset);
        if (str.length() < 256 * 1024) {
            return () -> new ByteArrayInputStream(str.getBytes(charset));
        }
        return () -> new ReaderInputStream(new StringReader(str), charset, 8 * 1024);
    }
}
