package net.dongliu.xhttp.body;

import net.dongliu.commons.collection.Collections2;
import net.dongliu.xhttp.ContentType;
import net.dongliu.xhttp.MimeType;
import net.dongliu.xhttp.Param;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

/**
 * Utils method to create body
 */
public class Bodies {

    /**
     * Create request body sending plain text.
     */
    public static Body<String> plainText(String value, Charset charset) {
        return of(value, MimeType.TEXT_PLAIN, charset);
    }

    /**
     * Create octet-stream request body.
     */
    public static Body<byte[]> octetStream(byte[] value) {
        return of(value, MimeType.OCTET_STREAM);
    }

    /**
     * Create octet-stream request body.
     */
    public static Body<InputSupplier> octetStream(InputSupplier inputSupplier) {
        return of(inputSupplier, MimeType.OCTET_STREAM);
    }

    /**
     * Create octet-stream request body.
     */
    public static Body<Path> octetStream(Path path) {
        return of(path, MimeType.OCTET_STREAM);
    }

    /**
     * Create request body sending plain text, with UTF-8 charset.
     */
    public static Body<String> plainText(String value) {
        return plainText(value, UTF_8);
    }

    /**
     * Create request body for sending json data
     */
    public static <T> Body<T> json(T value, Charset charset) {
        return new JsonBody<>(value, charset);
    }

    /**
     * Create request body for sending json data, with UTF-8 charset.
     */
    public static <T> Body<T> json(T value) {
        return json(value, UTF_8);
    }

    /**
     * Create request body send x-www-form-encoded data
     */
    public static Body<List<Param>> wwwForm(Collection<? extends Param> params, Charset charset) {
        return new FormBody(List.copyOf(requireNonNull(params)), charset);
    }

    /**
     * Create request body send x-www-form-encoded data, with UTF-8 charset.
     */
    public static Body<List<Param>> wwwForm(Collection<? extends Param> params) {
        return wwwForm(params, UTF_8);
    }

    /**
     * Create request body send x-www-form-encoded data
     */
    public static Body<List<Param>> wwwForm(Map<String, String> map, Charset charset) {
        var params = Collections2.convertToList(map.entrySet(), e -> Param.of(e.getKey(), e.getValue()));
        return new FormBody(params, charset);
    }

    /**
     * Create request body send x-www-form-encoded data, with UTF-8 charset.
     */
    public static Body<List<Param>> wwwForm(Map<String, String> params) {
        return wwwForm(params, UTF_8);
    }

    /**
     * Create request body send x-www-form-encoded data, with UTF-8 charset.
     */
    public static Body<List<Param>> wwwForm(Param... params) {
        requireNonNull(params);
        return new FormBody(List.of(params), UTF_8);
    }

    /**
     * Create multi-part encoded request body, from several Parts.
     */
    public static Body<List<Part>> multiPart(List<Part> parts) {
        return new MultiPartBody(List.copyOf(requireNonNull(parts)));
    }

    /**
     * Create multi-part encoded request body, from several Parts.
     */
    public static Body<List<Part>> multiPart(Part... parts) {
        requireNonNull(parts);
        return new MultiPartBody(List.of(parts));
    }

    /**
     * Create request body from byte array data
     *
     * @param mimeType the mime-type of body
     */
    public static Body<byte[]> of(byte[] value, MimeType mimeType) {
        return new BinaryBody(requireNonNull(value), ContentType.of(mimeType));
    }

    /**
     * Create request body from byte array data
     *
     * @param mimeType the mime-type of body
     * @param charset  the charset encoding of body
     */
    public static Body<byte[]> of(byte[] value, MimeType mimeType, Charset charset) {
        return new BinaryBody(requireNonNull(value), ContentType.of(mimeType, charset));
    }

    /**
     * Create request body send string data
     *
     * @param mimeType the mime-type of body
     * @param charset  the charset encoding of body
     */
    public static Body<String> of(String value, MimeType mimeType, Charset charset) {
        return new StringBody(requireNonNull(value), ContentType.of(mimeType, charset));
    }

    /**
     * Create request body from input stream.
     *
     * @param mimeType the mime-type of body
     */
    public static Body<InputSupplier> of(InputSupplier supplier, MimeType mimeType) {
        return new InputStreamBody(requireNonNull(supplier), ContentType.of(mimeType));
    }

    /**
     * Create request body from input stream.
     *
     * @param mimeType the mime-type of body
     * @param charset  the charset encoding of body
     */
    public static Body<InputSupplier> of(InputSupplier supplier, MimeType mimeType,
                                         Charset charset) {
        return new InputStreamBody(requireNonNull(supplier), ContentType.of(mimeType, charset));
    }

    /**
     * Create request body from file
     *
     * @param mimeType the mime-type of body
     */
    public static Body<Path> of(Path path, MimeType mimeType) {
        return new FileBody(requireNonNull(path), ContentType.of(mimeType));
    }

    /**
     * Create request body from file
     *
     * @param mimeType the mime-type of body
     * @param charset  the charset encoding of body
     */
    public static Body<Path> of(Path path, MimeType mimeType, Charset charset) {
        return new FileBody(requireNonNull(path), ContentType.of(mimeType, charset));
    }

}
