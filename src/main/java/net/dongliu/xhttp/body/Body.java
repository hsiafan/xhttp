package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;

/**
 * A http request body.
 *
 * @param <T> the body content type
 */
public interface Body<T> {

    /**
     * Return the body data.
     *
     * @return the body
     */
    T body();

    /**
     * The content type.
     *
     * @return the content type
     */
    ContentType contentType();

    /**
     * Given a charset, return a new body Publisher for sending request body.
     *
     * @return the Body Publisher
     */
    BodyPublisher asBodyPublisher(BodyPublishContext context);
}
