package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;
import net.dongliu.xhttp.MimeType;
import net.dongliu.xhttp.Param;
import net.dongliu.xhttp.utils.URIEncoder;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.nio.charset.Charset;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Post www-form-encoded body
 *
 * @author Liu Dong
 */
class FormBody extends AbstractBody<List<Param>> {

    FormBody(List<Param> body, Charset charset) {
        super(body, ContentType.of(MimeType.WWW_FORM_ENCODED, charset));
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        Charset charset = contentType().charset().orElse(UTF_8);
        String content = URIEncoder.encodeForms(body(), charset);
        return BodyPublishers.ofInputStream(InputSuppliers.of(content, charset));
    }
}
