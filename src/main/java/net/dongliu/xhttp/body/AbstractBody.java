package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;

import static java.util.Objects.requireNonNull;

/**
 * HTTPRequest body parent class
 *
 * @author Liu Dong
 */
abstract class AbstractBody<T> implements Body<T> {
    private final T body;
    private final ContentType contentType;

    /**
     * Construct a new Body.
     *
     * @param body        the body data
     * @param contentType the content type
     */
    protected AbstractBody(T body, ContentType contentType) {
        this.body = requireNonNull(body);
        this.contentType = requireNonNull(contentType);
    }

    @Override
    public T body() {
        return body;
    }

    @Override
    public ContentType contentType() {
        return contentType;
    }

    /**
     * Return body Publisher for sending request body.
     *
     * @return the Body Publisher
     */
    public abstract BodyPublisher asBodyPublisher(BodyPublishContext context);
}
