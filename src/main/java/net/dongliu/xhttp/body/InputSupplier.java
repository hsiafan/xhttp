package net.dongliu.xhttp.body;

import java.io.InputStream;
import java.util.function.Supplier;

/**
 * InputStream Supplier
 */
public interface InputSupplier extends Supplier<InputStream> {
    /**
     * Return a input stream for read.
     * Implementations should return a new, reproducible InputStream instance, when each time call this method.
     */
    @Override
    InputStream get();
}
