package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;

import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * HTTPRequest Body with text content.
 *
 * @author Liu Dong
 */
class StringBody extends AbstractBody<String> {

    StringBody(String body, ContentType contentType) {
        super(body, contentType);
    }

    @Override
    public BodyPublisher asBodyPublisher(BodyPublishContext context) {
        var charset = contentType().charset().orElse(UTF_8);
        return BodyPublishers.ofInputStream(InputSuppliers.of(body(), charset));
    }
}
