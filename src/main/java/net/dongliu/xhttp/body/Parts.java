package net.dongliu.xhttp.body;

import net.dongliu.xhttp.ContentType;
import net.dongliu.xhttp.MimeType;

import java.nio.file.Path;

/**
 * Utils for create {@link Part}
 */
public class Parts {
    /**
     * Create a file multi-part form field. The Text Part just as html input field:
     * <pre>
     *   &lt;input type="file" name="the_name"&gt;
     * </pre>
     *
     * @param name     the part name
     * @param path     the file path
     * @param mimeType the mimeType
     */
    public static Part<InputSupplier> file(String name, Path path, MimeType mimeType) {
        return new Part.FilePart(name, path.toString(), InputSuppliers.of(path), ContentType.of(mimeType));
    }

    /**
     * Create a file multi-part form field. The Text Part just as html input field:
     * <pre>
     *   &lt;input type="file" name="the_name"&gt;
     * </pre>
     * The mime-type will be detected from file name extension. If detect failed, use {@link MimeType#OCTET_STREAM}
     *
     * @param name the part name
     * @param path the file path
     */
    public static Part<InputSupplier> file(String name, Path path) {
        var mimeType = Files2.detectFileType(path).orElse(MimeType.OCTET_STREAM);
        return file(name, path, mimeType);
    }

    /**
     * Create a file multi-part form field. The Text Part just as html
     * <pre>
     *   &lt;input type="file" name="the_name"&gt;
     * </pre>
     * do.
     *
     * @param name     the part name
     * @param filename the filename, can be empty
     * @param supplier the file content provider
     * @param mimeType the mimeType
     */
    public static Part<InputSupplier> file(String name, String filename, InputSupplier supplier, MimeType mimeType) {
        return new Part.FilePart(name, filename, supplier, ContentType.of(mimeType));
    }

    /**
     * Create a file multi-part form field. The Text Part just as html
     * <pre>
     *   &lt;input type="file" name="the_name"&gt;
     * </pre>
     * do.
     *
     * @param name     the part name
     * @param filename the filename, can be empty
     * @param bytes    the file content
     * @param mimeType the mimeType
     */
    public static Part<InputSupplier> file(String name, String filename, byte[] bytes, MimeType mimeType) {
        return new Part.FilePart(name, filename, InputSuppliers.of(bytes), ContentType.of(mimeType));
    }

    /**
     * Create a text multi-part form field. The Text Part just as html
     * <pre>
     *       &lt;input type="text" name="the_name" value="the_value"&gt;
     * </pre>
     * do.
     */
    public static Part<String> text(String name, String value) {
        return new Part.TextPart(name, value);
    }
}
