package net.dongliu.xhttp;

import java.net.URL;

/**
 * Http HTTPRequest builder
 *
 * @author Liu Dong
 */
public class HTTPRequestBuilder extends AbstractRequestBuilder<HTTPRequestBuilder> {

    HTTPRequestBuilder(String method, URL url) {
        super(method, url);
    }

    @Override
    protected HTTPRequestBuilder self() {
        return this;
    }
}