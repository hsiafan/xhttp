package net.dongliu.xhttp.utils;

import net.dongliu.commons.annotation.InternalUseOnly;

import java.time.Duration;

/**
 * Only for internal use. utils for check arguments.
 */
@InternalUseOnly
public class Check {
    public static Duration timeout(Duration timeout) {
        if (timeout.isNegative() || timeout.isZero()) {
            throw new IllegalArgumentException("Invalid duration: " + timeout);
        }
        return timeout;
    }
}
