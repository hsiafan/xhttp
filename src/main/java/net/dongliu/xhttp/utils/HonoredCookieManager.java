package net.dongliu.xhttp.utils;

import net.dongliu.commons.annotation.InternalUseOnly;
import net.dongliu.commons.collection.Lists;
import net.dongliu.xhttp.HTTPHeaderNames;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * CookieManager that do not erase user setting cookies.
 */
@InternalUseOnly
public class HonoredCookieManager extends CookieManager {
    public HonoredCookieManager() {
        super();
    }

    public HonoredCookieManager(CookieStore store, CookiePolicy cookiePolicy) {
        super(store, cookiePolicy);
    }

    @Override
    public Map<String, List<String>> get(URI uri, Map<String, List<String>> requestHeaders) throws IOException {
        var headers = super.get(uri, requestHeaders);

        var userList = requestHeaders.getOrDefault(HTTPHeaderNames.COOKIE, List.of());
        var sessionList = headers.getOrDefault(HTTPHeaderNames.COOKIE, List.of());
        var cookieValues = Lists.concat(userList, sessionList);
        if (cookieValues.isEmpty()) {
            return Map.of();
        }
        return Map.of(HTTPHeaderNames.COOKIE, cookieValues);

    }
}
