package net.dongliu.xhttp.utils;

import net.dongliu.commons.annotation.InternalUseOnly;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * HostnameVerifier that do not check anything.
 * Seems no way to set HostnameVerifier to HttpClient?
 *
 * @author Liu Dong
 */
@InternalUseOnly
public class NopHostnameVerifier implements HostnameVerifier {

    private static class Holder {
        private static NopHostnameVerifier instance = new NopHostnameVerifier();
    }

    public static HostnameVerifier getInstance() {
        return Holder.instance;
    }

    @Override
    public boolean verify(String s, SSLSession sslSession) {
        return true;
    }
}
