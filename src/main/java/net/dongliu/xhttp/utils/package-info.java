/**
 * Utils classes, Only for internal use!
 *
 * @author Liu Dong
 */
@InternalUseOnly
package net.dongliu.xhttp.utils;

import net.dongliu.commons.annotation.InternalUseOnly;