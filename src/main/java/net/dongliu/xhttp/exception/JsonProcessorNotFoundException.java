package net.dongliu.xhttp.exception;

/**
 * @author Liu Dong
 */
public class JsonProcessorNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -5416319717741876095L;

    public JsonProcessorNotFoundException() {
    }
}
