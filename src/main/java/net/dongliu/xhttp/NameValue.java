package net.dongliu.xhttp;

/**
 * Immutable name-value pair, the name and value cannot be null.
 *
 * @author Liu Dong
 */
public interface NameValue {

    /**
     * If name equals the given name.
     */
    default boolean nameEquals(String name) {
        return this.name().equals(name);
    }

    /**
     * Return the name
     *
     * @return the name
     */
    String name();

    /**
     * Return the value
     */
    String value();

    @Override
    String toString();

    @Override
    int hashCode();

    @Override
    boolean equals(Object obj);
}
