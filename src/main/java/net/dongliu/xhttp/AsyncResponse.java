package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.exception.JsonProcessorNotFoundException;
import net.dongliu.xhttp.json.JsonProcessor;
import net.dongliu.xhttp.json.ReactiveJsonProcessor;
import net.dongliu.xhttp.json.TypeToken;

import java.lang.reflect.Type;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodySubscriber;
import java.net.http.HttpResponse.BodySubscribers;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Flow.Publisher;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

/**
 * Async Http Response.
 */
public class AsyncResponse {
    private final CompletableFuture<HttpResponse<Publisher<List<ByteBuffer>>>> responseFuture;
    private final List<Interceptor> interceptors;
    @Nullable
    private final JsonProcessor jsonProcessor;
    // user specified charset
    @Nullable
    private Charset charset = null;


    /**
     * Only for internal implementation.
     */
    public AsyncResponse(CompletableFuture<HttpResponse<Publisher<List<ByteBuffer>>>> responseFuture,
                         List<Interceptor> interceptors, @Nullable JsonProcessor jsonProcessor) {
        this.responseFuture = responseFuture;
        this.interceptors = interceptors;
        this.jsonProcessor = jsonProcessor;
    }

    /**
     * Set response read charset.
     * If not set, would get charset from response headers. If not found, would use UTF-8.
     *
     * @param charset the charset used to decode response body. Cannot be null
     */
    public AsyncResponse charset(Charset charset) {
        this.charset = requireNonNull(charset);
        return this;
    }

    /**
     * Handler async response body with a async body handler.
     *
     * @param handler the async body handler.
     * @param <T>     the body type returned by body handler
     * @return the response future
     */
    public <T> CompletableFuture<HTTPResponse<T>> handle(Handler<T> handler) {
        requireNonNull(handler);
        return responseFuture.thenCompose(r -> {
            var headers = HTTPHeaders.ofHttpHeaders(r.headers());
            var responseInfo = new Info(r.statusCode(), headers, r.body());
            for (var interceptor : interceptors) {
                responseInfo = interceptor.onAsyncResponse(responseInfo);
            }
            var bodyFuture = handler.handle(responseInfo);
            return bodyFuture.thenApply(body -> new HTTPResponse<>(
                    r.uri().toString(),
                    r.statusCode(),
                    HTTPHeaders.ofHttpHeaders(r.headers()),
                    body
            ));
        });
    }

    /**
     * Handler async response body with body subscriber.
     *
     * @param bodySubscriber the body subscriber.
     * @param <T>            the body type returned by body handler
     * @return the response future
     */
    public <T> CompletableFuture<HTTPResponse<T>> handle(BodySubscriber<T> bodySubscriber) {
        requireNonNull(bodySubscriber);
        return handle(ri -> {
            ri.body().subscribe(bodySubscriber);
            return bodySubscriber.getBody().toCompletableFuture();
        });
    }

    /**
     * Async response body info.
     */
    public static class Info {
        private final int statusCode;
        private final HTTPHeaders headers;
        private final Publisher<List<ByteBuffer>> body;

        public Info(int statusCode, HTTPHeaders headers, Publisher<List<ByteBuffer>> publisher) {
            this.statusCode = statusCode;
            this.headers = headers;
            this.body = publisher;
        }

        /**
         * the http response status
         */
        public int statusCode() {
            return statusCode;
        }

        /**
         * the http response headers
         */
        public HTTPHeaders headers() {
            return headers;
        }

        /**
         * the http body as ByteBuffer Publisher
         */
        public Publisher<List<ByteBuffer>> body() {
            return body;
        }
    }

    /**
     * Async body handler.
     *
     * @param <T> the body returned by this handler
     */
    @FunctionalInterface
    public interface Handler<T> {

        /**
         * Handle response, return the future of body. Implementation should return the future immediately.
         *
         * @param info the async response info
         * @return the future of body
         */
        CompletableFuture<T> handle(Info info);
    }

    /**
     * Treat the response body as String.
     *
     * @return the response Future
     */
    public CompletableFuture<HTTPResponse<String>> toTextResponse() {
        return handle(ri -> {
            Charset c = getCharset(ri);
            var bodySubscriber = BodySubscribers.ofString(c);
            ri.body().subscribe(bodySubscriber);
            return bodySubscriber.getBody().toCompletableFuture();
        });
    }

    /**
     * Collect the response body to byte array.
     *
     * @return the response Future
     */
    public CompletableFuture<HTTPResponse<byte[]>> toBinaryResponse() {
        return handle(BodySubscribers.ofByteArray());
    }

    /**
     * Decode response body as json, to value.
     *
     * @param <T> type value type
     * @return the value
     */
    public <T> CompletableFuture<HTTPResponse<T>> decodeJson(Class<T> cls) {
        requireNonNull(cls);
        var processor = ensureJsonProcessor();
        return handle(ri -> handleJson(cls, processor, ri));
    }

    /**
     * Decode response body as json, to value.
     *
     * @param <T> type value type
     * @return the value
     */
    public <T> CompletableFuture<HTTPResponse<T>> decodeJson(TypeToken<T> typeToken) {
        requireNonNull(typeToken);
        var processor = ensureJsonProcessor();
        return handle(ri -> handleJson(typeToken.getType(), processor, ri));
    }

    private JsonProcessor ensureJsonProcessor() {
        if (jsonProcessor == null) {
            throw new JsonProcessorNotFoundException();
        }
        return jsonProcessor;
    }

    private <T> CompletableFuture<T> handleJson(Type type, JsonProcessor processor, Info ri) {
        Charset c = getCharset(ri);
        var subscriber = ReactiveJsonProcessor.delegate(processor).<T>subscriberOf(c, type);
        ri.body().subscribe(subscriber);
        return subscriber.getBody().toCompletableFuture();
    }

    /**
     * Discard all response body.
     *
     * @return the response Future
     */
    public CompletableFuture<HTTPResponse<Void>> discard() {
        return handle(BodySubscribers.discarding());
    }

    /**
     * Write response body to file.
     *
     * @param path the file path
     * @return the response future
     */
    public CompletableFuture<HTTPResponse<Path>> writeTo(Path path) {
        return handle(BodySubscribers.ofFile(path));
    }

    private Charset getCharset(Info info) {
        if (charset != null) {
            return charset;
        }
        return info.headers().contentType().flatMap(ContentType::charset).orElse(UTF_8);
    }
}
