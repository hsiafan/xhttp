/**
 * Internal interceptor impl. Do not call api in this package.
 */
@InternalUseOnly
package net.dongliu.xhttp.interceptor;

import net.dongliu.commons.annotation.InternalUseOnly;