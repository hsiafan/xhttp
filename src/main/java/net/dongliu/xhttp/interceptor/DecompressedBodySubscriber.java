package net.dongliu.xhttp.interceptor;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

/**
 * Delegated Body Subscriber which decompress response content.
 */
class DecompressedBodySubscriber implements Subscriber<List<ByteBuffer>> {

    private final Subscriber<? super List<ByteBuffer>> subscriber;
    private final AsyncInflater asyncInflater;

    public DecompressedBodySubscriber(Subscriber<? super List<ByteBuffer>> subscriber, int wrapper) {
        this.subscriber = subscriber;
        this.asyncInflater = new AsyncInflater(wrapper);
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        subscriber.onSubscribe(subscription);
    }

    @Override
    public void onNext(List<ByteBuffer> item) {
        var buffers = new ArrayList<ByteBuffer>();
        for (var in : item) {
            asyncInflater.decode(in, buffers::add);
        }
        subscriber.onNext(buffers);
    }

    @Override
    public void onError(Throwable throwable) {
        asyncInflater.onFinish();
        subscriber.onError(throwable);
    }

    @Override
    public void onComplete() {
        asyncInflater.onFinish();
        subscriber.onComplete();
    }
}
