package net.dongliu.xhttp.interceptor;

import net.dongliu.commons.annotation.InternalUseOnly;
import net.dongliu.xhttp.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow.Publisher;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import static java.util.Collections.unmodifiableList;
import static net.dongliu.xhttp.HTTPHeaderNames.ACCEPT_ENCODING;
import static net.dongliu.xhttp.HTTPHeaderNames.CONTENT_ENCODING;
import static net.dongliu.xhttp.HTTPMethods.HEAD;
import static net.dongliu.xhttp.StatusCodes.NOT_MODIFIED;
import static net.dongliu.xhttp.StatusCodes.NO_CONTENT;

/**
 * A interceptor send accept encoding gzip, and auto handle gzip response decompress.
 */
@InternalUseOnly
public class GzipResponseInterceptor implements Interceptor {
    private volatile String method = null;

    @Override
    public HTTPRequest onRequest(HTTPRequest request) {
        method = request.method();
        var headers = request.headers();
        var newHeaders = new ArrayList<HTTPHeader>(request.headers().size() + 1);
        for (HTTPHeader header : headers) {
            if (!header.nameEquals(ACCEPT_ENCODING)) {
                newHeaders.add(header);
            }
        }
        newHeaders.add(HTTPHeader.of(ACCEPT_ENCODING, "gzip, deflate"));
        return request.toBuilder().headers(unmodifiableList(newHeaders)).build();
    }

    @Override
    public BlockingResponse.Info onBlockingResponse(BlockingResponse.Info response) {
        try {
            var body = wrapCompressedInput(response.statusCode(), response.headers(), response.body());
            return new BlockingResponse.Info(response.statusCode(), response.headers(), body);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Wrap response input stream if it is compressed
     */
    private InputStream wrapCompressedInput(int status, HTTPHeaders headers, InputStream input)
            throws IOException {
        // if has no body, some server still set content-encoding header,
        // GZIPInputStream wrap empty input stream will cause exception. we should check this
        if (noBody(status)) {
            return input;
        }

        var contentEncoding = headers.getHeader(CONTENT_ENCODING).orElse("");

        //we should remove the content-encoding header here?
        switch (contentEncoding) {
            case "gzip":
                return new GZIPInputStream(input);
            case "deflate":
                // Note: deflate implements may or may not wrap in zlib due to rfc confusing.
                // here deal with deflate without zlib header
                return new InflaterInputStream(input, new Inflater(true));
            case "identity":
            default:
                return input;
        }
    }

    private boolean noBody(int status) {
        if (HEAD.equals(method) || (status >= 100 && status < 200) || status == NOT_MODIFIED || status == NO_CONTENT) {
            return true;
        }
        return false;
    }

    @Override
    public AsyncResponse.Info onAsyncResponse(AsyncResponse.Info response) {
        var publisher = wrapCompressedPublisher(response.statusCode(), response.headers(), response.body());
        return new AsyncResponse.Info(response.statusCode(), response.headers(), publisher);
    }

    private Publisher<List<ByteBuffer>> wrapCompressedPublisher(int status, HTTPHeaders headers,
                                                                Publisher<List<ByteBuffer>> publisher) {
        if (noBody(status)) {
            return publisher;
        }
        var contentEncoding = headers.getHeader(CONTENT_ENCODING).orElse("").trim();
        switch (contentEncoding) {
            case "gzip":
                return subscriber -> publisher.subscribe(new DecompressedBodySubscriber(subscriber, AsyncInflater.GZIP));
            case "deflate":
                // Note: deflate implements may or may not wrap in zlib due to rfc confusing.
                // here deal with deflate without zlib header
                return subscriber -> publisher.subscribe(new DecompressedBodySubscriber(subscriber, AsyncInflater.ZLIB));
            case "identity":
            default:
                return publisher;
        }
    }
}
