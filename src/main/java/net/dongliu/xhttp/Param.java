package net.dongliu.xhttp;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Immutable parameter, for uri query string, and www-form-encoded parameters.
 * The name and value cannot be null.
 *
 * @author Liu Dong
 */
public class Param implements NameValue {
    private final String name;
    private final String value;

    private Param(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Create new param.
     *
     * @param name  the name, cannot be null
     * @param value the value, cannot be null
     * @return the param
     */
    public static Param of(String name, String value) {
        return new Param(requireNonNull(name), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Param param = (Param) o;
        return Objects.equals(name, param.name) &&
                Objects.equals(value, param.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, value);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return name + '=' + value;
    }
}
