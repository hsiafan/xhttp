package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.body.Body;

import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

/**
 * Http request
 *
 * @author Liu Dong
 */
public class HTTPRequest {
    private final String method;
    private final List<HTTPHeader> headers;
    private final List<HTTPCookie> cookies;
    private final List<Param> params;

    private final Charset paramCharset;
    @Nullable
    private final Body<?> body;
    private final boolean autoGzip;
    @Nullable
    private final PasswordAuthentication basicAuth;
    private final URL url;

    @Nullable
    private final String userAgent;
    @Nullable
    private final String referer;
    private final Duration timeout;

    HTTPRequest(AbstractRequestBuilder<?> builder) {
        method = builder.method;
        url = builder.url;
        headers = builder.headers;
        cookies = builder.cookies;
        userAgent = builder.userAgent;
        referer = builder.referer;
        paramCharset = builder.paramCharset;
        body = builder.body;
        autoGzip = builder.autoGzip;
        basicAuth = builder.basicAuth;
        params = builder.params;
        timeout = builder.timeout;
    }

    /**
     * Create new HTTPRequest builder, all fields in builder are set by value of this HTTPRequest.
     *
     * @return the HTTPRequestBuilder
     */
    public HTTPRequestBuilder toBuilder() {
        HTTPRequestBuilder builder = new HTTPRequestBuilder(method, url);
        builder.headers = headers;
        builder.cookies = cookies;
        builder.userAgent = userAgent;
        builder.referer = referer;
        builder.paramCharset = paramCharset;
        builder.body = body;
        builder.autoGzip = autoGzip;
        builder.basicAuth = basicAuth;
        builder.params = params;
        builder.timeout = timeout;
        return builder;
    }

    public String method() {
        return method;
    }

    public List<HTTPHeader> headers() {
        return headers;
    }

    /**
     * The cookie set by user.
     */
    public List<HTTPCookie> cookies() {
        return cookies;
    }

    /**
     * The user set user-agent (in HTTPClient or in HTTPRequest). If not any custom user-agent was set, return empty optional.
     */
    public Optional<String> userAgent() {
        return Optional.ofNullable(userAgent);
    }

    /**
     * The referer set by user. If not any custom user-agent was set, return empty optional.
     */
    public Optional<String> referer() {
        return Optional.ofNullable(referer);
    }

    /**
     * The charset used to encode params to url query string.
     */
    public Charset paramCharset() {
        return paramCharset;
    }

    public Optional<Body<?>> body() {
        return Optional.ofNullable(body);
    }

    public boolean autoGzip() {
        return autoGzip;
    }

    /**
     * Basic auth for this request.
     *
     * @return if not exist, return empty Optional
     */
    public Optional<PasswordAuthentication> basicAuth() {
        return Optional.ofNullable(basicAuth);
    }

    public URL url() {
        return url;
    }

    public Duration timeout() {
        return timeout;
    }

    /**
     * Parameter to append to url.
     */
    public List<Param> params() {
        return params;
    }

}
