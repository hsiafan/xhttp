package net.dongliu.xhttp;

import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * HTTP mime type.
 */
public class MimeType {
    private final String type;
    private final String subType;

    // text types
    public static final MimeType TEXT_PLAIN = MimeType.of("text", "plain");
    public static final MimeType HTML = MimeType.of("text", "html");
    public static final MimeType CSS = MimeType.of("text", "css");
    public static final MimeType CSV = MimeType.of("text", "csv");


    // application textual types
    public static final MimeType JAVA_SCRIPT = MimeType.of("application", "javascript");
    public static final MimeType JSON = MimeType.of("application", "json");
    public static final MimeType XHTML = MimeType.of("application", "xhtml+xml");
    public static final MimeType XML = MimeType.of("application", "xml");

    // application binary types
    public static final MimeType OCTET_STREAM = MimeType.of("application", "octet-stream");
    public static final MimeType MS_WORD = MimeType.of("application", "msword");
    public static final MimeType MS_PPT = MimeType.of("application", "vnd.ms-powerpoint");
    public static final MimeType MS_EXCEL = MimeType.of("application", "vnd.ms-excel"); // Microsoft Excel
    public static final MimeType MS_EXCEL_OPEN_XML =
            MimeType.of("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"); // Microsoft Excel (OpenXML)
    public static final MimeType VISIO = MimeType.of("application", "vnd.visio");
    public static final MimeType EPUB = MimeType.of("application", "epub+zip");
    public static final MimeType PDF = MimeType.of("application", "pdf");
    public static final MimeType RTF = MimeType.of("application", "rtf");
    public static final MimeType FLASH = MimeType.of("application", "x-shockwave-flash");
    public static final MimeType TTF = MimeType.of("application", "x-font-ttf"); // TrueType Font
    public static final MimeType WOFF = MimeType.of("application", "x-font-woff"); // Web Open Font Format
    public static final MimeType OGG = MimeType.of("application", "ogg");
    public static final MimeType JAR = MimeType.of("application", "java-archive");
    public static final MimeType ZIP = MimeType.of("application", "zip");
    public static final MimeType BZIP = MimeType.of("application", "x-bzip");
    public static final MimeType BZIP2 = MimeType.of("application", "x-bzip2");
    public static final MimeType TAR = MimeType.of("application", "x-tar");
    public static final MimeType _7Z = MimeType.of("application", "x-7z-compressed");
    public static final MimeType RAR = MimeType.of("application", "x-rar-compressed");

    // image types
    public static final MimeType JPEG = MimeType.of("image", "jpeg");
    public static final MimeType GIF = MimeType.of("image", "gif");
    public static final MimeType ICON = MimeType.of("image", "x-icon");
    public static final MimeType SVG = MimeType.of("image", "svg+xml");
    public static final MimeType TIFF = MimeType.of("image", "tiff");
    public static final MimeType WEBP = MimeType.of("image", "webp");

    // audio types
    public static final MimeType AAC = MimeType.of("audio", "aac");
    public static final MimeType MIDI = MimeType.of("audio", "midi");
    public static final MimeType OGG_AUDIO = MimeType.of("audio", "ogg");
    public static final MimeType WAV = MimeType.of("audio", "x-wav");
    public static final MimeType WEBM_AUDIO = MimeType.of("audio", "webm");
    public static final MimeType _3GPP_AUDIO = MimeType.of("audio", "3gpp");

    // video types
    public static final MimeType MPEG = MimeType.of("video", "mpeg");
    public static final MimeType OGG_VIDEO = MimeType.of("video", "ogg");
    public static final MimeType WEBM_VIDEO = MimeType.of("video", "webm");
    public static final MimeType _3GPP_VIDEO = MimeType.of("video", "3gpp");
    public static final MimeType _3GPP_VIDEO2 = MimeType.of("video", "3gpp2");

    // multi part
    public static final MimeType MULTI_PART = MimeType.of("multipart", "form-data");
    // www-form-encoded
    public static final MimeType WWW_FORM_ENCODED = MimeType.of("application", "x-www-form-urlencoded");

    private MimeType(String type, String subType) {
        this.type = type;
        this.subType = subType;
    }

    /**
     * Create new mime type instance, by type and subType.
     *
     * @param type    the mime type
     * @param subType the mime sub type
     * @return a MimeType instance
     */
    public static MimeType of(String type, String subType) {
        return new MimeType(requireNonNull(type), requireNonNull(subType));
    }

    /**
     * Parse mime type string.
     *
     * @param mimeType the mime-type string
     * @return If parse failed, return empty Optional.
     */
    public static Optional<MimeType> safeParse(String mimeType) {
        Objects.requireNonNull(mimeType);
        if (mimeType.isEmpty()) {
            return Optional.empty();
        }
        int idx = mimeType.indexOf("/");
        if (idx == -1) {
            return Optional.empty();
        }
        String type = mimeType.substring(0, idx);
        String subType = mimeType.substring(idx + 1);
        if (type.isEmpty() || subType.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new MimeType(type, subType));
    }

    /**
     * The mime type
     */
    public String type() {
        return type;
    }

    /**
     * The mime subType
     */
    public String subType() {
        return subType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MimeType mimeType = (MimeType) o;
        return Objects.equals(type, mimeType.type) &&
                Objects.equals(subType, mimeType.subType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, subType);
    }

    @Override
    public String toString() {
        return type + "/" + subType;
    }
}
