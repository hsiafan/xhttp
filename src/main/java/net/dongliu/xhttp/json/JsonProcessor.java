package net.dongliu.xhttp.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodySubscriber;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow.Subscription;

import static java.util.Objects.requireNonNull;

/**
 * Json processor, used to decode/encode json values, blocking and non-blocking.
 *
 * @author Liu Dong
 */
public interface JsonProcessor {

    /**
     * Serialize value to json, and write to writer. The output stream is leaved unclosed.
     */
    void marshal(Object value, OutputStream out, Charset charset) throws IOException;

    /**
     * Deserialize json from Reader, to specific type. The input stream is closed when finished or error occurred.
     *
     * @param charset the charset, cannot be null
     */
    <T> T unmarshal(InputStream in, Charset charset, Type type) throws IOException;

    /**
     * Deserialize json from Reader, to specific class type T. The input stream is closed when finished or error occurred.
     *
     * @param charset the charset, cannot be null
     */
    default <T> T unmarshal(InputStream in, Charset charset, Class<T> cls) throws IOException {
        return unmarshal(in, charset, (Type) cls);
    }

    /**
     * Deserialize json from Reader, to specific type T. The input stream is closed when finished or error occurred.
     *
     * @param charset the charset, cannot be null
     */
    default <T> T unmarshal(InputStream in, Charset charset, TypeToken<T> token) throws IOException {
        return unmarshal(in, charset, token.getType());
    }

}
