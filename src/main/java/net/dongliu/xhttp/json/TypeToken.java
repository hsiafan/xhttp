package net.dongliu.xhttp.json;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Simple generic type infer class, for tell json processor which type to decode with json.
 *
 * @param <T>
 */
public abstract class TypeToken<T> {
    final Type type;

    protected TypeToken() {
        this.type = getSuperclassTypeParameter(getClass());
    }

    private static Type getSuperclassTypeParameter(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (superclass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        ParameterizedType parameterized = (ParameterizedType) superclass;
        return parameterized.getActualTypeArguments()[0];
    }

    /**
     * Gets underlying {@code Type} instance.
     */
    public final Type getType() {
        return type;
    }
}
