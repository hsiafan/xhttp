package net.dongliu.xhttp.json;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import static java.nio.charset.StandardCharsets.*;
import static java.util.Objects.requireNonNull;

/**
 * Provider json ability via jackson
 *
 * @author Liu Dong
 */
public class JacksonJsonProcessor implements JsonProcessor {

    private final ObjectMapper objectMapper;

    /**
     * Create jackson json processor, using default setting
     */
    public JacksonJsonProcessor() {
        this(createDefault());
    }

    private static ObjectMapper createDefault() {
        return new ObjectMapper().findAndRegisterModules();
    }

    /**
     * Create jackson json processor, passing one objectMapper
     *
     * @param objectMapper the ObjectMapper
     */
    public JacksonJsonProcessor(ObjectMapper objectMapper) {
        this.objectMapper = requireNonNull(objectMapper);
    }

    @Override
    public void marshal(Object value, OutputStream out, Charset charset) throws IOException {
        if (charset.equals(UTF_8)) {
            objectMapper.writeValue(out, value);
        } else {
            objectMapper.writeValue(new OutputStreamWriter(out, charset), value);
        }
    }

    @Override
    public <T> T unmarshal(InputStream in, Charset charset, Type type) throws IOException {
        requireNonNull(in);
        requireNonNull(charset);
        requireNonNull(type);
        if (isJsonCharset(charset)) {
            try (in) {
                JavaType javaType = objectMapper.getTypeFactory().constructType(type);
                return objectMapper.readValue(in, javaType);
            }
        } else {
            try (in; var reader = new InputStreamReader(in, charset)) {
                JavaType javaType = objectMapper.getTypeFactory().constructType(type);
                return objectMapper.readValue(reader, javaType);
            }
        }
    }

    private boolean isJsonCharset(Charset charset) {
        // US_ASCII compatible with UTF_8
        return charset.equals(UTF_8) || charset.equals(UTF_16) || charset.equals(US_ASCII);
    }
}
