package net.dongliu.xhttp.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.Type;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpResponse.BodySubscriber;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;

import static java.util.Objects.requireNonNull;

/**
 * ReactiveJsonProcessor implementation that delegate to ordinary {@link JsonProcessor}
 */
class DelegateReactiveJsonProcessor implements ReactiveJsonProcessor {
    private final JsonProcessor jsonProcessor;

    DelegateReactiveJsonProcessor(JsonProcessor jsonProcessor) {
        this.jsonProcessor = jsonProcessor;
    }

    /**
     * Marshal value to json, for async http using. Default using block api to get all data once, and then publish.
     *
     * @param value   the value
     * @param charset the charset
     * @return http body publisher
     */
    @Override
    public BodyPublisher asPublisher(Object value, Charset charset) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            jsonProcessor.marshal(value, bos, charset);
            return HttpRequest.BodyPublishers.ofByteArray(bos.toByteArray());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Return a BodySubscriber, which deserialize json from ByteBuffers async, with charset and type.
     * Default impl read all data of response body unblocking, and then unmarshal using blocking api.
     * Override this method if implementation can decode json with non-blocking API.
     */
    @Override
    public <T> BodySubscriber<T> subscriberOf(Charset charset, Type type) {
        //TODO: defer response body decompress to onComplete stage
        requireNonNull(charset);
        requireNonNull(type);
        return new BodySubscriber<>() {
            private final CompletableFuture<T> future = new CompletableFuture<>();
            private Flow.Subscription subscription;
            private final List<ByteBuffer> buffers = new ArrayList<>();

            @Override
            public CompletionStage<T> getBody() {
                return future;
            }

            @Override
            public void onSubscribe(Flow.Subscription subscription) {
                this.subscription = subscription;
                subscription.request(1);
            }

            @Override
            public void onNext(List<ByteBuffer> item) {
                buffers.addAll(item);
                subscription.request(1);
            }

            @Override
            public void onError(Throwable throwable) {
                future.completeExceptionally(throwable);
                subscription.cancel();
            }

            @Override
            public void onComplete() {
                try (var in = new ByteBuffersInputStream(buffers)) {
                    future.complete(jsonProcessor.unmarshal(in, charset, type));
                } catch (Throwable t) {
                    future.completeExceptionally(t);
                }
            }
        };
    }
}
