/**
 * Json adapter, wrapping jackson, and may be other libs in future.
 *
 * @author Liu Dong
 */
package net.dongliu.xhttp.json;