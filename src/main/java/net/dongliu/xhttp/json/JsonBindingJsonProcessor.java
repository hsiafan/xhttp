package net.dongliu.xhttp.json;

import net.dongliu.commons.Lazy;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import static java.util.Objects.requireNonNull;

/**
 * Json Processor impl by JSON-B API
 */
public class JsonBindingJsonProcessor implements JsonProcessor {

    private final Jsonb jsonb;

    /**
     * Create json processor with default jsonb.
     */
    public JsonBindingJsonProcessor() {
        this(defaultJsonb.get());
    }

    /**
     * Create json processor with jsonb instance.
     *
     * @param jsonb the jsonb
     */
    public JsonBindingJsonProcessor(Jsonb jsonb) {
        this.jsonb = requireNonNull(jsonb);
    }

    private static final Lazy<Jsonb> defaultJsonb = Lazy.of(JsonbBuilder::create);

    @Override
    public void marshal(Object value, OutputStream out, Charset charset) throws IOException {
        var writer = new OutputStreamWriter(out, charset);
        jsonb.toJson(value, writer);
    }

    @Override
    public <T> T unmarshal(InputStream in, Charset charset, Type type) throws IOException {
        try (var reader = new InputStreamReader(in, charset)) {
            return jsonb.fromJson(reader, type);
        }
    }
}
