package net.dongliu.xhttp.json;

import com.fasterxml.jackson.core.JsonParser.NumberType;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.async.ByteArrayFeeder;
import com.fasterxml.jackson.core.async.ByteBufferFeeder;
import com.fasterxml.jackson.core.json.async.NonBlockingJsonParser;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;

import static com.fasterxml.jackson.databind.DeserializationFeature.*;
import static java.util.Objects.requireNonNull;

/**
 * Async deserializer that can build instances of {@link JsonNode} from any
 * JSON content, using appropriate {@link JsonNode} type.
 */
class JacksonAsyncJsonNodeDeserializer {

    private final DeserializationConfig deserializationConfig;
    private final NonBlockingJsonParser parser;
    private final ByteBufferFeeder feeder;
    private final Deque<JsonNode> stack = new ArrayDeque<>();
    private String fieldName;
    // the final node root. this node is set when parse finished.
    private JsonNode rootNode;

    JacksonAsyncJsonNodeDeserializer(ObjectMapper mapper) throws IOException {
        this.deserializationConfig = mapper.getDeserializationConfig();
        var jsonFactory = mapper.getFactory();
        this.parser = (NonBlockingJsonParser) jsonFactory.createNonBlockingByteArrayParser();
        var byteArrayFeeder = parser.getNonBlockingInputFeeder();
        this.feeder = new DelegateByteBufferFeeder(byteArrayFeeder);
    }


    /**
     * feed data into this deserializer.
     *
     * @param buffer the data buffer
     * @throws IOException when io error
     */
    public void feed(ByteBuffer buffer) throws IOException {
        requireNonNull(buffer);
        if (this.rootNode != null) {
            throw new IOException("json stream already end");
        }
        boolean consumed = false;
        while (!consumed) {
            if (feeder.needMoreInput()) {
                feeder.feedInput(buffer);
                consumed = true;
            }

            JsonToken token;
            while ((token = parser.nextToken()) != null) {
                if (token == JsonToken.NOT_AVAILABLE) {
                    break;
                }
                buildTree(token);
            }
        }
    }

    /**
     * Complete the json parser. return the json node.
     */
    public JsonNode complete() throws IOException {
        feeder.endOfInput();
        JsonToken token;
        while ((token = parser.nextToken()) != null) {
            if (token == JsonToken.NOT_AVAILABLE) {
                break;
            }
            buildTree(token);
        }
        if (rootNode == null) {
            throw new IOException("unexpected end of json stream");
        }
        return this.rootNode;
    }

    private void buildTree(JsonToken token) throws IOException {
        switch (token) {

            case NOT_AVAILABLE:
                // should not happen
                break;
            case START_OBJECT:
                onStartObject();
                break;
            case END_OBJECT:
                onEndObject();
                break;
            case START_ARRAY:
                onStartArray();
                break;
            case END_ARRAY:
                onEndArray();
                break;
            case FIELD_NAME:
                this.fieldName = parser.getCurrentName();
                break;
            case VALUE_EMBEDDED_OBJECT:
                break;
            case VALUE_STRING:
                onStringValue();
                break;
            case VALUE_NUMBER_INT:
                onIntValue();
                break;
            case VALUE_NUMBER_FLOAT:
                onFloatValue();
                break;
            case VALUE_TRUE:
                onBooleanValue(true);
                break;
            case VALUE_FALSE:
                onBooleanValue(false);
                break;
            case VALUE_NULL:
                onNullValue();
                break;
        }
    }

    private void onStartObject() throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            var node = nodeFactory().objectNode();
            stack.push(node);
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            var node = ((ObjectNode) parent).putObject(name);
            stack.push(node);
        } else if (parent instanceof ArrayNode) {
            var node = ((ArrayNode) parent).addObject();
            stack.push(node);
        } else {
            throwIllegalParent(parent);
        }
    }

    private void onEndObject() throws IOException {
        if (stack.isEmpty()) {
            // what?
            setRoot(nodeFactory().objectNode());
        } else {
            var node = stack.pop();
            if (stack.isEmpty()) {
                setRoot(node);
            }
        }
    }

    private void onStartArray() throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            var node = nodeFactory().arrayNode();
            stack.push(node);
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            var node = ((ObjectNode) parent).putArray(name);
            stack.push(node);
        } else if (parent instanceof ArrayNode) {
            var node = ((ArrayNode) parent).addArray();
            stack.push(node);
        } else {
            throwIllegalParent(parent);
        }
    }

    private void onEndArray() throws IOException {
        if (stack.isEmpty()) {
            // what?
            setRoot(nodeFactory().arrayNode());
        } else {
            var node = stack.pop();
            if (stack.isEmpty()) {
                setRoot(node);
            }
        }
    }

    private void onStringValue() throws IOException {
        var text = parser.getText();
        var parent = stack.peek();
        if (parent == null) {
            setRoot(nodeFactory().textNode(text));
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            ((ObjectNode) parent).put(name, text);
        } else if (parent instanceof ArrayNode) {
            ((ArrayNode) parent).add(text);
        } else {
            throwIllegalParent(parent);
        }
    }

    private void onIntValue() throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            setRoot(fromInt());
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            ((ObjectNode) parent).set(name, fromInt());
        } else if (parent instanceof ArrayNode) {
            ((ArrayNode) parent).add(fromInt());
        } else {
            throwIllegalParent(parent);
        }
    }

    protected final JsonNode fromInt() throws IOException {
        NumberType nt;
        int feats = deserializationFeatures();
        if (USE_BIG_INTEGER_FOR_INTS.enabledIn(feats)) {
            nt = NumberType.BIG_INTEGER;
        } else if (USE_LONG_FOR_INTS.enabledIn(feats)) {
            nt = NumberType.LONG;
        } else {
            nt = parser.getNumberType();
        }
        if (nt == NumberType.INT) {
            return nodeFactory().numberNode(parser.getIntValue());
        }
        if (nt == NumberType.LONG) {
            return nodeFactory().numberNode(parser.getLongValue());
        }
        return nodeFactory().numberNode(parser.getBigIntegerValue());
    }

    private void onFloatValue() throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            setRoot(fromFloat());
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            ((ObjectNode) parent).set(name, fromFloat());
        } else if (parent instanceof ArrayNode) {
            ((ArrayNode) parent).add(fromFloat());
        } else {
            throwIllegalParent(parent);
        }
    }

    protected final JsonNode fromFloat() throws IOException {
        NumberType nt = parser.getNumberType();
        if (nt == NumberType.BIG_DECIMAL) {
            return nodeFactory().numberNode(parser.getDecimalValue());
        }
        int feats = deserializationFeatures();
        if (USE_BIG_DECIMAL_FOR_FLOATS.enabledIn(feats)) {
            if (parser.isNaN()) {
                return nodeFactory().numberNode(parser.getDoubleValue());
            }
            return nodeFactory().numberNode(parser.getDecimalValue());
        }
        if (nt == NumberType.FLOAT) {
            return nodeFactory().numberNode(parser.getFloatValue());
        }
        return nodeFactory().numberNode(parser.getDoubleValue());
    }

    private void onBooleanValue(boolean value) throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            setRoot(nodeFactory().booleanNode(value));
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            ((ObjectNode) parent).put(name, value);
        } else if (parent instanceof ArrayNode) {
            ((ArrayNode) parent).add(value);
        } else {
            throwIllegalParent(parent);
        }
    }

    private void onNullValue() throws IOException {
        var parent = stack.peek();
        if (parent == null) {
            setRoot(nodeFactory().nullNode());
        } else if (parent instanceof ObjectNode) {
            String name = consumeFieldName();
            ((ObjectNode) parent).putNull(name);
        } else if (parent instanceof ArrayNode) {
            ((ArrayNode) parent).addNull();
        } else {
            throwIllegalParent(parent);
        }
    }

    private void setRoot(JsonNode node) throws IOException {
        if (rootNode != null) {
            throw new IOException("already has root node");
        }
        this.rootNode = node;
    }

    private String consumeFieldName() {
        String name = requireNonNull(fieldName);
        fieldName = null;
        return name;
    }

    private JsonNodeFactory nodeFactory() {
        return deserializationConfig.getNodeFactory();
    }

    private int deserializationFeatures() {
        return deserializationConfig.getDeserializationFeatures();
    }

    private void throwIllegalParent(JsonNode parent) throws IOException {
        throw new IOException("parent node is not array or object, but: " + parent.getNodeType());
    }


    private static class DelegateByteBufferFeeder implements ByteBufferFeeder {

        private final ByteArrayFeeder byteArrayFeeder;

        private DelegateByteBufferFeeder(ByteArrayFeeder byteArrayFeeder) {
            this.byteArrayFeeder = byteArrayFeeder;
        }

        @Override
        public void feedInput(ByteBuffer buffer) throws IOException {
            if (buffer.hasArray()) {
                byteArrayFeeder.feedInput(buffer.array(), buffer.arrayOffset() + buffer.position(),
                        buffer.arrayOffset() + buffer.limit());
            } else {
                byte[] array = new byte[buffer.remaining()];
                buffer.get(array);
                byteArrayFeeder.feedInput(array, 0, array.length);
            }
        }

        @Override
        public boolean needMoreInput() {
            return byteArrayFeeder.needMoreInput();
        }

        @Override
        public void endOfInput() {
            byteArrayFeeder.endOfInput();
        }
    }

}
