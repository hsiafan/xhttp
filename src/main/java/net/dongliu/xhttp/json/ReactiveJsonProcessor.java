package net.dongliu.xhttp.json;

import java.lang.reflect.Type;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpResponse.BodySubscriber;
import java.nio.charset.Charset;

/**
 * Non-blocking json processor with reactive API.
 */
public interface ReactiveJsonProcessor {
    /**
     * Marshal value to json, for async http using. Default using block api to get all data once, and then publish.
     *
     * @param value   the value
     * @param charset the charset
     * @return http body publisher
     */
    BodyPublisher asPublisher(Object value, Charset charset);

    /**
     * Return a BodySubscriber, which deserialize json from ByteBuffers async, with charset and type.
     * Default impl read all data of response body unblocking, and then unmarshal using blocking api.
     * Override this method if implementation can decode json with non-blocking API.
     */
    <T> BodySubscriber<T> subscriberOf(Charset charset, Type type);

    /**
     * Return a BodySubscriber, which deserialize json from ByteBuffers async, with charset and class type.
     */
    default <T> BodySubscriber<T> subscriberOf(Charset charset, Class<T> cls) {
        return subscriberOf(charset, (Type) cls);
    }

    /**
     * Return a BodySubscriber, which deserialize json from ByteBuffers async, with charset and type.
     */
    default <T> BodySubscriber<T> subscriberOf(Charset charset, TypeToken<T> typeToken) {
        return subscriberOf(charset, typeToken.getType());
    }

    //TODO: move to JsonLookup
    static ReactiveJsonProcessor delegate(JsonProcessor jsonProcessor) {
        return new DelegateReactiveJsonProcessor(jsonProcessor);
    }
}
