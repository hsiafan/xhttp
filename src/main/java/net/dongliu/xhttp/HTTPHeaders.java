package net.dongliu.xhttp;


import net.dongliu.commons.Lazy;
import net.dongliu.commons.Optionals;
import net.dongliu.commons.annotation.Nullable;
import net.dongliu.commons.collection.Lists;

import java.io.Serializable;
import java.net.HttpCookie;
import java.net.http.HttpHeaders;
import java.time.Instant;
import java.util.*;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toUnmodifiableList;
import static net.dongliu.xhttp.HTTPHeaderNames.CONTENT_TYPE;

/**
 * The http headers.
 * This class is thread-safe.
 *
 * @author Liu Dong
 */
public class HTTPHeaders implements Serializable {
    private static final long serialVersionUID = -1283402589869346874L;
    private final Map<String, List<String>> map;
    private final Lazy<List<HTTPCookie>> lazyCookies;
    private final Lazy<@Nullable ContentType> lazyContentType;

    /**
     * Create a new HTTPHeaders.
     *
     * @param map the headers map
     */
    private HTTPHeaders(Map<String, List<String>> map) {
        this.map = requireNonNull(map);
        this.lazyCookies = Lazy.of(this::parseCookies);
        this.lazyContentType = Lazy.of(() -> getHeader(CONTENT_TYPE).flatMap(ContentType::safeParse).orElse(null));
    }

    /**
     * Construct a headers, from immutable jdk http client headers.
     *
     * @param httpHeaders the http headers
     * @return HTTPHeaders
     */
    public static HTTPHeaders ofHttpHeaders(HttpHeaders httpHeaders) {
        return new HTTPHeaders(httpHeaders.map());
    }

    /**
     * Construct headers from a Collection of header.
     *
     * @param headers the header collection
     * @return HTTPHeaders
     */
    public static HTTPHeaders of(Collection<? extends HTTPHeader> headers) {
        var map = new HashMap<String, List<String>>();
        for (var header : headers) {
            map.computeIfAbsent(header.name(), name -> new ArrayList<>(2)).add(header.value());
        }
        return new HTTPHeaders(copyCaseInsensitive(map));
    }

    // copy map to a new immutable, case-insensitive map
    private static Map<String, List<String>> copyCaseInsensitive(Map<String, List<String>> map) {
        var newMap = new TreeMap<String, List<String>>(String.CASE_INSENSITIVE_ORDER);
        for (var entry : map.entrySet()) {
            newMap.put(entry.getKey(), List.copyOf(entry.getValue()));
        }
        return unmodifiableMap(newMap);
    }

    private List<HTTPCookie> parseCookies() {
        var cookieValues = getHeaders(HTTPHeaderNames.SET_COOKIE);
        // hope timestamp do not diff too much
        var createTime = Instant.now();
        return cookieValues.stream()
                .flatMap(v -> HttpCookie.parse(v).stream())
                .map(c -> new HTTPCookie(c.getName(), c.getValue(), c.getDomain(), c.getPath(),
                        c.getMaxAge() < 0 ? OptionalLong.empty() : OptionalLong.of(c.getMaxAge()),
                        c.getSecure(), c.isHttpOnly(), createTime))
                .collect(toUnmodifiableList());
    }

    /**
     * Get headers by name. If not exists, return empty list
     */
    public List<String> getHeaders(String name) {
        requireNonNull(name);
        return map.getOrDefault(name, List.of());
    }

    /**
     * Get the first header value matched name.
     */
    public Optional<String> getHeader(String name) {
        requireNonNull(name);
        return Lists.first(getHeaders(name));
    }

    /**
     * Get header value as long.
     */
    public OptionalLong getLongHeader(String name) {
        var header = getHeader(name);
        return Optionals.mapToLong(header, Long::parseLong);
    }

    /**
     * Get all header as Map
     *
     * @return immutable map of headers. return empty map if do not has any header.
     */
    public Map<String, List<String>> headers() {
        return map;
    }

    /**
     * Get content type set in http header.
     * If do not set Content-Type header, or parse header error, return empty Optional.
     *
     * @return the charset
     */
    public Optional<ContentType> contentType() {
        return Optional.ofNullable(lazyContentType.get());
    }

    /**
     * Get cookies in this header.
     *
     * @return a immutable list of cookies
     */
    public List<HTTPCookie> cookies() {
        return lazyCookies.get();
    }

}
