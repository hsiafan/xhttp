package net.dongliu.xhttp;


import net.dongliu.commons.collection.Lists;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Http Response with body be transformed
 *
 * @author Liu Dong
 */
public class HTTPResponse<T> {
    private final String url;
    private final int statusCode;
    private final HTTPHeaders headers;
    private final T body;

    public HTTPResponse(String url, int statusCode, HTTPHeaders headers, T body) {
        this.url = requireNonNull(url);
        this.statusCode = statusCode;
        this.headers = requireNonNull(headers);
        // we do not check null here, cause for type Void we use null.
        this.body = body;
    }

    /**
     * Get actual url (redirected)
     */
    public String url() {
        return url;
    }

    /**
     * Return http response status code.
     *
     * @return the status code
     */
    public int statusCode() {
        return statusCode;
    }

    /**
     * Get all cookies returned by this response.
     *
     * @return a immutable list of cookies
     */
    public List<HTTPCookie> cookies() {
        return headers.cookies();
    }

    /**
     * Get all response headers
     */
    public HTTPHeaders headers() {
        return headers;
    }

    /**
     * Return the response body.
     *
     * @return The response body
     */
    public T body() {
        return body;
    }

    /**
     * Get first cookie match the name returned by this response, return null if not found
     */
    public Optional<HTTPCookie> getCookie(String name) {
        return Lists.find(cookies(), c -> c.nameEquals(name));
    }

    /**
     * Get first header value match the name, return null if not exists
     */
    public Optional<String> getHeader(String name) {
        return headers.getHeader(name);
    }

    /**
     * Get all headers values with name. If not exists, return empty list.
     *
     * @return a immutable list of header values
     */
    public List<String> getHeaders(String name) {
        return this.headers.getHeaders(name);
    }
}
