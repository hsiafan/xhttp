package net.dongliu.xhttp;

import net.dongliu.commons.collection.Lists;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.List;

/**
 * For ProxySelector
 */
public class ProxySelectors {

    private static final List<Proxy> DIRECT_PROXIES = List.of(Proxy.NO_PROXY);

    /**
     * Return ProxySelector that always use the same proxy
     *
     * @param proxy the proxy
     * @return proxy selector
     */
    public static ProxySelector staticSelector(Proxy proxy) {
        return new StaticProxySelector(Lists.of(proxy));
    }


    /**
     * ProxySelector always use the same proxy
     */
    static class StaticProxySelector extends ProxySelector {
        private final List<Proxy> list;

        StaticProxySelector(List<Proxy> list) {
            this.list = list;
        }

        @Override
        public void connectFailed(URI uri, SocketAddress sa, IOException e) {
            /* ignore */
        }

        @Override
        public synchronized List<Proxy> select(URI uri) {
            String scheme = uri.getScheme().toLowerCase();
            if (scheme.equals("http") || scheme.equals("https")) {
                return list;
            } else {
                return DIRECT_PROXIES;
            }
        }
    }
}
