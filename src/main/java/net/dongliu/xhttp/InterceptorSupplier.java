package net.dongliu.xhttp;

import java.util.function.Supplier;

/**
 * Factory to create Interceptor, for each request.
 */
@FunctionalInterface
public interface InterceptorSupplier extends Supplier<Interceptor> {

    /**
     * This method will called when a new request is executed.
     *
     * @return interceptor
     */
    @Override
    Interceptor get();
}
