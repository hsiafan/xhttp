package net.dongliu.xhttp;

import net.dongliu.commons.Lazy;
import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.exception.RequestsException;
import net.dongliu.xhttp.json.JsonProcessor;
import net.dongliu.xhttp.utils.SSLContextFactories;

import java.net.*;
import java.net.http.HttpClient;
import java.security.KeyStore;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;

import static java.net.http.HttpClient.Redirect.ALWAYS;
import static java.net.http.HttpClient.Redirect.NEVER;
import static java.net.http.HttpClient.Version.HTTP_1_1;
import static java.net.http.HttpClient.Version.HTTP_2;
import static java.util.Objects.requireNonNull;

/**
 * The http client.
 */
public class HTTPClient {
    @Nullable
    private final ProxySelector proxySelector;
    private final boolean useHttp2;
    private final boolean verifyCert;
    private final boolean followRedirect;
    @Nullable
    private final Authenticator authenticator;
    private final CookieHandler cookieHandler;
    @Nullable
    private final Executor executor;
    @Nullable
    private final KeyStore keyStore;
    private final Duration connectTimeout;

    private final Duration timeout;
    @Nullable
    private final String userAgent;
    private final boolean autoGzip;
    @Nullable
    private final JsonProcessor jsonProcessor;

    final HttpClient httpClient;
    private final List<InterceptorSupplier> interceptorSuppliers;

    HTTPClient(HTTPClientBuilder clientBuilder) {
        this.proxySelector = clientBuilder.proxySelector;
        this.useHttp2 = clientBuilder.useHttp2;
        this.verifyCert = clientBuilder.verifyCert;
        this.followRedirect = clientBuilder.followRedirect;
        this.timeout = clientBuilder.timeout;
        this.userAgent = clientBuilder.userAgent;
        this.autoGzip = clientBuilder.autoGzip;
        this.authenticator = clientBuilder.authenticator;
        this.cookieHandler = clientBuilder.cookieHandler;
        this.keyStore = clientBuilder.keyStore;
        this.connectTimeout = clientBuilder.connectTimeout;

        this.executor = clientBuilder.executor;
        this.interceptorSuppliers = List.copyOf(clientBuilder.interceptorSuppliers);
        this.jsonProcessor = clientBuilder.jsonProcessor;

        var builder = HttpClient.newBuilder();
        if (!verifyCert) {
            builder.sslContext(SSLContextFactories.getTrustAllSSLContext());
        } else if (keyStore != null) {
            builder.sslContext(SSLContextFactories.getCustomTrustSSLContext(keyStore));
        }

        if (executor != null) {
            builder.executor(executor);
        }
        builder.version(useHttp2 ? HTTP_2 : HTTP_1_1)
                .followRedirects(followRedirect ? ALWAYS : NEVER);
        if (proxySelector != null) {
            builder.proxy(proxySelector);
        }
        if (authenticator != null) {
            builder.authenticator(authenticator);
        }
        builder.cookieHandler(cookieHandler);
        builder.connectTimeout(connectTimeout);

        this.httpClient = builder.build();
    }

    /**
     * Get a new HttpClient Builder
     */
    public static HTTPClientBuilder builder() {
        return new HTTPClientBuilder();
    }


    private static final Lazy<HTTPClient> defaultClient = Lazy.of(() -> new HTTPClientBuilder().build());

    /**
     * Return the default Http Client with default settings. The client is shared across one jvm process.
     */
    public static HTTPClient defaultClient() {
        return defaultClient.get();
    }

    /**
     * Start a GET request
     */
    public HttpRequestContext get(URL url) {
        return newRequest(HTTPMethods.GET, url);
    }

    /**
     * Start a POST request
     */
    public HttpRequestContext post(URL url) {
        return newRequest(HTTPMethods.POST, url);
    }

    /**
     * Start a PUT request
     */
    public HttpRequestContext put(URL url) {
        return newRequest(HTTPMethods.PUT, url);
    }

    /**
     * Start a DELETE request
     */
    public HttpRequestContext delete(URL url) {
        return newRequest(HTTPMethods.DELETE, url);
    }

    /**
     * Start a HEAD request
     */
    public HttpRequestContext head(URL url) {
        return newRequest(HTTPMethods.HEAD, url);
    }

    /**
     * Create new request with method and url
     */
    public HttpRequestContext newRequest(String method, URL url) {
        var builder = new HttpRequestContext(this, method, url).timeout(timeout).autoGzip(autoGzip);
        if (userAgent != null) {
            builder.userAgent(userAgent);
        }
        return builder;
    }

    /**
     * Start a GET request
     */
    public HttpRequestContext get(String url) {
        return newRequest(HTTPMethods.GET, url);
    }

    /**
     * Start a POST request
     */
    public HttpRequestContext post(String url) {
        return newRequest(HTTPMethods.POST, url);
    }

    /**
     * Start a PUT request
     */
    public HttpRequestContext put(String url) {
        return newRequest(HTTPMethods.PUT, url);
    }

    /**
     * Start a DELETE request
     */
    public HttpRequestContext delete(String url) {
        return newRequest(HTTPMethods.DELETE, url);
    }

    /**
     * Start a HEAD request
     */
    public HttpRequestContext head(String url) {
        return newRequest(HTTPMethods.HEAD, url);
    }

    /**
     * Create new request with method and url
     */
    public HttpRequestContext newRequest(String method, String urlStr) {
        URL url;
        try {
            url = new URL(requireNonNull(urlStr));
        } catch (MalformedURLException e) {
            throw new RequestsException("Resolve url error, url: " + urlStr, e);
        }
        return newRequest(method, url);
    }

    public Optional<ProxySelector> proxy() {
        return Optional.ofNullable(proxySelector);
    }

    public boolean useHttp2() {
        return useHttp2;
    }

    public boolean verifyCert() {
        return verifyCert;
    }

    public boolean followRedirect() {
        return followRedirect;
    }

    public Duration timeout() {
        return timeout;
    }

    public Duration connectTimeout() {
        return connectTimeout;
    }

    /**
     * The user set user-agent. If user do not set a custom user-agent, return empty Optional.
     */
    public Optional<String> userAgent() {
        return Optional.ofNullable(userAgent);
    }

    public boolean autoGzip() {
        return autoGzip;
    }

    public Optional<Authenticator> authenticator() {
        return Optional.ofNullable(authenticator);
    }

    public CookieHandler cookieHandler() {
        return cookieHandler;
    }

    public Optional<KeyStore> keyStore() {
        return Optional.ofNullable(keyStore);
    }

    /**
     * The use set custom executor for http executing. Return empty optional if use do not set a Executor.
     */
    public Optional<Executor> executor() {
        return Optional.ofNullable(executor);
    }

    public List<InterceptorSupplier> interceptors() {
        return interceptorSuppliers;
    }

    /**
     * The JsonProcessor set to this client. Return empty optional if no JsonProcessor is set.
     */
    public Optional<JsonProcessor> jsonProcessor() {
        return Optional.ofNullable(jsonProcessor);
    }
}
