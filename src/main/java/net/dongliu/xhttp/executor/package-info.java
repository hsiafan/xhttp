/**
 * Process http request.
 * Only for internal implementation, DO NOT use this package.
 */
@InternalUseOnly
package net.dongliu.xhttp.executor;

import net.dongliu.commons.annotation.InternalUseOnly;