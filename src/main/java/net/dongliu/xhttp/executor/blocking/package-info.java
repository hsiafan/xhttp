/**
 * Blocking Http request process.
 * Only for internal implementation, DO NOT use this package.
 */
@InternalUseOnly
package net.dongliu.xhttp.executor.blocking;

import net.dongliu.commons.annotation.InternalUseOnly;