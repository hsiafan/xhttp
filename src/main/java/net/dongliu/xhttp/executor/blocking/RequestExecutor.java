package net.dongliu.xhttp.executor.blocking;

import net.dongliu.xhttp.BlockingResponse;
import net.dongliu.xhttp.HTTPRequest;

/**
 * Interface to work with Interceptor, represent a process routine for request.
 */
public interface RequestExecutor {
    /**
     * Process the request, and return response
     */
    BlockingResponse proceed(HTTPRequest request);
}
