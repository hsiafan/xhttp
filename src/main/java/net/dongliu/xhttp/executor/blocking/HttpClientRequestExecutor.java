package net.dongliu.xhttp.executor.blocking;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.commons.collection.Lists;
import net.dongliu.xhttp.BlockingResponse;
import net.dongliu.xhttp.HTTPHeaders;
import net.dongliu.xhttp.HTTPRequest;
import net.dongliu.xhttp.InterceptorSupplier;
import net.dongliu.xhttp.body.BodyPublishContext;
import net.dongliu.xhttp.exception.RequestsException;
import net.dongliu.xhttp.executor.RequestUtils;
import net.dongliu.xhttp.json.JsonProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;

/**
 * Send request, and get response, in blocked way.
 */
public class HttpClientRequestExecutor implements RequestExecutor {

    private final HttpClient httpClient;
    private final List<InterceptorSupplier> interceptorSuppliers;
    @Nullable
    private final JsonProcessor jsonProcessor;

    public HttpClientRequestExecutor(HttpClient httpClient, List<InterceptorSupplier> interceptorSuppliers,
                                     @Nullable JsonProcessor jsonProcessor) {
        this.httpClient = httpClient;
        this.interceptorSuppliers = interceptorSuppliers;
        this.jsonProcessor = jsonProcessor;
    }

    @Override
    public BlockingResponse proceed(HTTPRequest request) {
        // interceptors
        var interceptors = Lists.convert(interceptorSuppliers, InterceptorSupplier::get);
        for (var interceptor : interceptors) {
            request = interceptor.onRequest(request);
        }

        // send
        var httpRequest = RequestUtils.toHttpRequest(request, new BodyPublishContext(jsonProcessor));
        HttpResponse<InputStream> response;
        try {
            response = httpClient.send(httpRequest, BodyHandlers.ofInputStream());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            // this may be the best we can do to handle InterruptedException except declare throwing in method signature
            // mark the interrupt bit
            Thread.currentThread().interrupt();
            throw new RequestsException("HTTP Request has been interrupted");
        }

        int status = response.statusCode();
        var httpHeaders = response.headers();
        HTTPHeaders headers = HTTPHeaders.ofHttpHeaders(httpHeaders);
        InputStream in = response.body();
        var url = response.uri().toString();
        var responseInfo = new BlockingResponse.Info(status, headers, in);
        for (var interceptor : interceptors) {
            responseInfo = interceptor.onBlockingResponse(responseInfo);
        }
        return new BlockingResponse(url, responseInfo, jsonProcessor);
    }


}
