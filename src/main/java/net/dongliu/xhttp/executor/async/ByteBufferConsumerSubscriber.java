package net.dongliu.xhttp.executor.async;


import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

/**
 * BodySubscriber that delegate body process to ByteBuffer consumer.
 */
public class ByteBufferConsumerSubscriber implements HttpResponse.BodySubscriber<Void> {
    private final Consumer<ByteBuffer> consumer;
    private Flow.Subscription subscription;
    private final CompletableFuture<Void> result = new CompletableFuture<>();
    private final AtomicBoolean subscribed = new AtomicBoolean();

    public ByteBufferConsumerSubscriber(Consumer<ByteBuffer> consumer) {
        this.consumer = requireNonNull(consumer);
    }

    @Override
    public CompletionStage<Void> getBody() {
        return result;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        if (!subscribed.compareAndSet(false, true)) {
            subscription.cancel();
        } else {
            this.subscription = subscription;
            subscription.request(1);
        }
    }

    @Override
    public void onNext(List<ByteBuffer> items) {
        for (ByteBuffer item : items) {
            consumer.accept(item);
        }
        subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        result.completeExceptionally(throwable);
    }

    @Override
    public void onComplete() {
        consumer.accept(null);
        result.complete(null);
    }

}
