package net.dongliu.xhttp.executor.async;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.commons.collection.Lists;
import net.dongliu.xhttp.AsyncResponse;
import net.dongliu.xhttp.HTTPRequest;
import net.dongliu.xhttp.InterceptorSupplier;
import net.dongliu.xhttp.body.BodyPublishContext;
import net.dongliu.xhttp.executor.RequestUtils;
import net.dongliu.xhttp.json.JsonProcessor;

import java.net.http.HttpClient;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;

/**
 * Async send http request and get response.
 */
public class AsyncHttpClientRequestExecutor {

    private final HttpClient httpClient;
    private final List<InterceptorSupplier> interceptorSuppliers;
    @Nullable
    private final JsonProcessor jsonProcessor;


    public AsyncHttpClientRequestExecutor(HttpClient httpClient, List<InterceptorSupplier> interceptorSuppliers,
                                          @Nullable JsonProcessor jsonProcessor) {
        this.httpClient = httpClient;
        this.interceptorSuppliers = interceptorSuppliers;
        this.jsonProcessor = jsonProcessor;
    }

    public AsyncResponse proceedAsync(HTTPRequest request) {
        // interceptors
        var interceptors = Lists.convert(interceptorSuppliers, InterceptorSupplier::get);
        for (var interceptor : interceptors) {
            request = interceptor.onRequest(request);
        }

        var httpRequest = RequestUtils.toHttpRequest(request, new BodyPublishContext(jsonProcessor));

        var responseFuture = httpClient.sendAsync(httpRequest, BodyHandlers.ofPublisher());

        return new AsyncResponse(responseFuture, interceptors, jsonProcessor);
    }
}
