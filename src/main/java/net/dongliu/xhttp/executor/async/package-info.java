/**
 * Process http request async.
 * Only for internal implementation, DO NOT use this package.
 */
@InternalUseOnly
package net.dongliu.xhttp.executor.async;

import net.dongliu.commons.annotation.InternalUseOnly;