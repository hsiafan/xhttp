package net.dongliu.xhttp.executor;

import net.dongliu.commons.annotation.InternalUseOnly;
import net.dongliu.xhttp.HTTPHeaderNames;
import net.dongliu.xhttp.HTTPMethods;
import net.dongliu.xhttp.HTTPRequest;
import net.dongliu.xhttp.body.BodyPublishContext;
import net.dongliu.xhttp.exception.RequestsException;
import net.dongliu.xhttp.utils.URIEncoder;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Utils method to make http request instance.
 */
@InternalUseOnly
public class RequestUtils {

    /**
     * Construct a HttpRequest from HTTPRequest instance.
     *
     * @param request the request
     * @return HttpRequest
     */
    public static HttpRequest toHttpRequest(HTTPRequest request, BodyPublishContext publishContext) {
        var body = request.body();

        var httpRequestBuilder = HttpRequest.newBuilder();
        var bodyPublisher = body
                .map(b -> b.asBodyPublisher(publishContext))
                .orElseGet(BodyPublishers::noBody);

        // HttpClient set bodyPublisher to null when method is GET.
        // For now (JDK 10.0.0), HttpClient may hang when send GET HTTPRequest with a empty body.
        switch (request.method()) {
            case HTTPMethods.GET:
                httpRequestBuilder.GET();
                break;
            case HTTPMethods.DELETE:
                httpRequestBuilder.DELETE();
                break;
            default:
                httpRequestBuilder.method(request.method(), bodyPublisher);
        }

        var url = URIEncoder.joinUrl(request.url(), request.params(), request.paramCharset());
        URI uri;
        try {
            uri = url.toURI();
        } catch (URISyntaxException e) {
            throw new RequestsException(e);
        }
        httpRequestBuilder.uri(uri);
        httpRequestBuilder.expectContinue(false);
        body.ifPresent(b -> {
            var contentType = b.contentType();
            httpRequestBuilder.setHeader(HTTPHeaderNames.CONTENT_TYPE, contentType.toString());
        });

        httpRequestBuilder.timeout(request.timeout());

        // headers
        request.userAgent().ifPresent(ua -> httpRequestBuilder.setHeader(HTTPHeaderNames.USER_AGENT, ua));

        request.referer().ifPresent(referer -> httpRequestBuilder.setHeader(HTTPHeaderNames.REFERER, referer));

        request.basicAuth().ifPresent(ba -> {
            var authStr = ba.getUserName() + ":" + new String(ba.getPassword());
            String auth = "Basic " + Base64.getEncoder().encodeToString(authStr.getBytes(UTF_8));
            httpRequestBuilder.setHeader(HTTPHeaderNames.AUTHORIZATION, auth);
        });

        // set user custom headers
        for (var header : request.headers()) {
            httpRequestBuilder.setHeader(header.name(), String.valueOf(header.value()));
        }

        if (request.autoGzip()) {
            httpRequestBuilder.setHeader(HTTPHeaderNames.ACCEPT_ENCODING, "gzip, deflate");
        }

        // set cookies
        if (!request.cookies().isEmpty()) {
            // When HTTP/1.1 is used, a single Cookie: header should be sent. When HTTP/2 is used, then multiple Cookie headers should be sent.
            // we just set multi cookie header, hope JDK HttpClient can send this headers to our custom CookieManager.
            for (var cookie : request.cookies()) {
                httpRequestBuilder.header(HTTPHeaderNames.COOKIE, cookie.name() + "=" + cookie.value());
            }
        }

        httpRequestBuilder.timeout(request.timeout());

        return httpRequestBuilder.build();
    }
}
