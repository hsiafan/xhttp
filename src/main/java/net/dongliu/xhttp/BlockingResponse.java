package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.commons.io.InputStreams;
import net.dongliu.commons.io.Readers;
import net.dongliu.xhttp.exception.JsonProcessorNotFoundException;
import net.dongliu.xhttp.json.JsonProcessor;
import net.dongliu.xhttp.json.TypeToken;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

/**
 * Raw blocking http response. The http headers already received, the http body not consumed, can be get as InputStream.
 * It you do not consume http response body, with {@link #toTextResponse()}, {@link #toBinaryResponse()},
 * {@link #decodeJson(Class)}, {@link #writeTo(Path)} etc.., you need to close this raw response manually.
 *
 * @author Liu Dong
 */
public class BlockingResponse implements AutoCloseable {
    protected final String url;
    protected final Info info;
    @Nullable
    private final JsonProcessor jsonProcessor;
    // user specified charset
    @Nullable
    private Charset charset = null;

    /**
     * Only for internal implementation.
     */
    public BlockingResponse(String url, Info info, @Nullable JsonProcessor jsonProcessor) {
        this.url = requireNonNull(url);
        this.info = info;
        this.jsonProcessor = jsonProcessor;
    }

    /**
     * Set response body read charset.
     * If not set, would get charset from response headers. If not found, would use UTF-8.
     *
     * @param charset the charset used to decode response body. Cannot be null
     */
    public BlockingResponse charset(Charset charset) {
        this.charset = requireNonNull(charset);
        return this;
    }

    /**
     * Handle response body with handler, return a new response with content as handler result.
     * ResponseHandler should consume all InputStream data, or connection may close and cannot reuse.
     * The response is closed whether this call succeed or failed with exception.
     */
    public <T> HTTPResponse<T> handle(Handler<T> handler) {
        try (info.body) {
            T result = handler.handle(info);
            return new HTTPResponse<>(url, info.statusCode(), info.headers(), result);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Convert to response, with body as text. The origin raw response will be closed
     */
    public HTTPResponse<String> toTextResponse() {
        Charset charset = getCharset();
        return handle(ri -> {
            try (Reader reader = new InputStreamReader(ri.body(), charset)) {
                return Readers.readAll(reader);
            }
        });
    }

    /**
     * Convert to response, with body as byte array
     */
    public HTTPResponse<byte[]> toBinaryResponse() {
        return handle(ri -> ri.body().readAllBytes());
    }

    /**
     * Convert http response body to json result
     */
    public <T> HTTPResponse<T> decodeJson(TypeToken<T> typeToken) {
        requireNonNull(typeToken);
        var processor = ensureJsonProcessor();
        Charset charset = getCharset();
        return handle(ri -> processor.unmarshal(ri.body(), charset, typeToken));
    }

    /**
     * Convert http response body to json result
     */
    public <T> HTTPResponse<T> decodeJson(Class<T> cls) {
        requireNonNull(cls);
        var processor = ensureJsonProcessor();
        Charset charset = getCharset();
        return handle(ri -> processor.unmarshal(ri.body(), charset, cls));
    }

    private JsonProcessor ensureJsonProcessor() {
        if (jsonProcessor == null) {
            throw new JsonProcessorNotFoundException();
        }
        return jsonProcessor;
    }

    /**
     * Write response body to file
     */
    public HTTPResponse<Path> writeTo(Path path) {
        return handle(ri -> {
            try (var out = Files.newOutputStream(path)) {
                ri.body().transferTo(out);
            }
            return null;
        });
    }

    /**
     * Write response body to OutputStream. OutputStream will not be closed.
     */
    public HTTPResponse<Void> writeTo(OutputStream out) {
        return handle(ri -> {
            ri.body().transferTo(out);
            return null;
        });
    }

    /**
     * Write response body to Writer, charset can be set using {@link #charset(Charset)},
     * or will use charset detected from response header if not set.
     * Writer will not be closed.
     */
    public HTTPResponse<Void> writeTo(Writer writer) {
        var charset = getCharset();
        return handle(ri -> {
            try (Reader reader = new InputStreamReader(ri.body(), charset)) {
                Readers.transferTo(reader, writer);
                return null;
            }
        });
    }

    /**
     * Consume and discard this response body.
     */
    public HTTPResponse<Void> discard() {
        return handle(ri -> {
            InputStreams.discardAll(ri.body());
            return null;
        });
    }

    private Charset getCharset() {
        if (charset != null) {
            return charset;
        }
        return info.headers().contentType().flatMap(ContentType::charset).orElse(UTF_8);
    }

    @Override
    public void close() {
        discard();
    }

    /**
     * Handle response in blocking way.
     */
    @FunctionalInterface
    public interface Handler<T> {
        /**
         * Handler response body, convert to instance of type T.
         *
         * @param info the response info
         * @return the value body convert to
         */
        T handle(Info info) throws IOException;
    }

    /**
     * For holding block response info.
     */
    public static class Info {
        private final int statusCode;
        private final HTTPHeaders headers;
        private final InputStream body;

        public Info(int statusCode, HTTPHeaders headers, InputStream body) {
            this.statusCode = statusCode;
            this.headers = headers;
            this.body = body;
        }

        /**
         * the http response status
         */
        public int statusCode() {
            return statusCode;
        }

        /**
         * the http response headers
         */
        public HTTPHeaders headers() {
            return headers;
        }

        /**
         * the http body as InputStream
         */
        public InputStream body() {
            return body;
        }
    }
}
