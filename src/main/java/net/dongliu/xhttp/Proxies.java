package net.dongliu.xhttp;

import java.net.InetSocketAddress;
import java.net.Proxy;

import static java.util.Objects.requireNonNull;

/**
 * Utils class for create Proxy
 *
 * @author Liu Dong
 */
public class Proxies {
    /**
     * Create http proxy
     */
    public static Proxy httpProxy(String host, int port) {
        return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(requireNonNull(host), port));
    }

    /**
     * Create socks5 proxy
     */
    public static Proxy socksProxy(String host, int port) {
        return new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(requireNonNull(host), port));
    }
}
