package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * The HTTP Content Type header value.
 */
public class ContentType {
    private final MimeType mimeType;
    /**
     * For text body.
     */
    @Nullable
    private final Charset charset;
    /**
     * For multi-part content type. use empty string for other content types.
     */
    private final String boundary;

    private ContentType(MimeType mimeType, @Nullable Charset charset, String boundary) {
        this.mimeType = mimeType;
        this.charset = charset;
        this.boundary = boundary;
    }

    /**
     * Create a ContentType, which do not care about charset encoding.
     *
     * @param mimeType the mime type
     * @return ContentType instance
     */
    public static ContentType of(MimeType mimeType) {
        requireNonNull(mimeType);
        return new ContentType(mimeType, null, "");
    }

    /**
     * Create a ContentType with a charset, used for Textual ContentType.
     *
     * @param mimeType the mime type
     * @param charset  the charset
     * @return ContentType instance
     */
    public static ContentType of(MimeType mimeType, Charset charset) {
        requireNonNull(mimeType);
        requireNonNull(charset);
        return new ContentType(mimeType, charset, "");
    }

    /**
     * Create a multi-part Content-Type.
     *
     * @param boundary the boundary
     * @return multi-part Content-Type instance.
     */
    public static ContentType multiPart(String boundary) {
        if (requireNonNull(boundary).isEmpty()) {
            throw new IllegalArgumentException("boundary is empty");
        }
        return new ContentType(MimeType.MULTI_PART, null, boundary);
    }

    /**
     * Parse from content type str.
     *
     * @param contentTypeStr the content-type str
     * @return If parse failed, return empty Optional.
     */
    public static Optional<ContentType> safeParse(String contentTypeStr) {
        requireNonNull(contentTypeStr);
        if (contentTypeStr.isEmpty()) {
            return Optional.empty();
        }

        String[] splits = contentTypeStr.split("; ");
        if (splits.length > 2) {
            return Optional.empty();
        }
        var mimeTypeOptional = MimeType.safeParse(splits[0]);
        if (mimeTypeOptional.isEmpty()) {
            return Optional.empty();
        }
        var mimeType = mimeTypeOptional.get();
        if (splits.length == 2) {
            String str = splits[1].trim();
            int idx = str.indexOf('=');
            if (idx == -1) {
                return Optional.empty();
            }
            String name = str.substring(0, idx);
            String value = str.substring(idx + 1);
            switch (name) {
                case "charset":
                    return Optional.of(new ContentType(mimeType, Charset.forName(value), ""));
                case "boundary":
                    return Optional.of(new ContentType(mimeType, null, value));
                default:
                    return Optional.empty();
            }
        } else {
            return Optional.of(ContentType.of(mimeType));
        }
    }

    /**
     * The mime type
     */
    public MimeType mimeType() {
        return mimeType;
    }

    /**
     * The charset set, only for textual content-type.
     */
    public Optional<Charset> charset() {
        return Optional.ofNullable(charset);
    }

    /**
     * The boundary, only for multi part content-type.
     */
    public String boundary() {
        return boundary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentType that = (ContentType) o;
        return Objects.equals(mimeType, that.mimeType) &&
                Objects.equals(charset, that.charset) &&
                Objects.equals(boundary, that.boundary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mimeType, charset, boundary);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(mimeType.toString());
        if (charset != null) {
            sb.append("; charset=").append(charset.name().toLowerCase());
        }
        if (!boundary.isEmpty()) {
            sb.append("; boundary=").append(boundary);
        }
        return sb.toString();
    }
}
