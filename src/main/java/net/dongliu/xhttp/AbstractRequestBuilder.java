package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.body.Body;
import net.dongliu.xhttp.utils.Check;

import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static net.dongliu.commons.collection.Collections2.convertToList;

abstract class AbstractRequestBuilder<T extends AbstractRequestBuilder> {
    final String method;
    final URL url;
    List<HTTPHeader> headers = List.of();
    List<HTTPCookie> cookies = List.of();
    List<Param> params = List.of();
    Charset paramCharset = StandardCharsets.UTF_8;
    @Nullable
    Body<?> body = null;
    @Nullable
    PasswordAuthentication basicAuth = null;

    // those fields can  override client settings
    Duration timeout = Duration.ofSeconds(10);
    @Nullable
    String userAgent = null;
    @Nullable
    String referer = null;
    boolean autoGzip = true;

    AbstractRequestBuilder(String method, URL url) {
        this.method = requireNonNull(method);
        this.url = requireNonNull(url);
    }

    /**
     * The timeout for one request to return response, do not include connect time.
     * This setting will override timeout setting in HttpClient.
     *
     * @param timeout must larger than zero
     */
    public T timeout(Duration timeout) {
        this.timeout = Check.timeout(timeout);
        return self();
    }

    /**
     * The request user-agent.
     * This setting will override timeout setting in HttpClient.
     *
     * @param userAgent cannot be null
     */
    public T userAgent(String userAgent) {
        this.userAgent = requireNonNull(userAgent);
        return self();
    }

    /**
     * Set request referer header.
     *
     * @param referer cannot be null
     */
    public T referer(String referer) {
        this.referer = requireNonNull(referer);
        return self();
    }

    /**
     * Set request headers.
     */
    public T headers(List<HTTPHeader> headers) {
        this.headers = requireNonNull(headers);
        return self();
    }

    /**
     * Set request headers.
     */
    public final T headers(HTTPHeader... headers) {
        headers(List.of(headers));
        return self();
    }

    /**
     * Set request headers.
     */
    public final T headers(Map<String, String> map) {
        this.headers = convertToList(map.entrySet(), e -> HTTPHeader.of(e.getKey(), e.getValue()));
        return self();
    }

    /**
     * Set request cookies.
     */
    public T cookies(List<HTTPCookie> cookies) {
        this.cookies = cookies;
        return self();
    }

    /**
     * Set request cookies.
     */
    public final T cookies(HTTPCookie... cookies) {
        cookies(List.of(cookies));
        return self();
    }

    /**
     * Set request cookies.
     */
    public final T cookies(Map<String, String> map) {
        this.cookies = convertToList(map.entrySet(), e -> HTTPCookie.of(e.getKey(), e.getValue()));
        return self();
    }

    /**
     * Set url query params.
     *
     * @param params the parameters
     */
    public T params(List<Param> params) {
        this.params = requireNonNull(params);
        return self();
    }

    /**
     * Set url query params.
     *
     * @param params  the parameters
     * @param charset charset used to encode params to query string
     */
    public T params(List<Param> params, Charset charset) {
        this.params = requireNonNull(params);
        this.paramCharset = charset;
        return self();
    }

    /**
     * Set url query params.
     *
     * @param params the parameters
     */
    public final T params(Param... params) {
        this.params = List.of(params);
        return self();
    }

    /**
     * Set url query params.
     *
     * @param map the parameters
     */
    public final T params(Map<String, String> map) {
        this.params = convertToList(map.entrySet(), e -> Param.of(e.getKey(), e.getValue()));
        return self();
    }

    /**
     * Set url query params.
     *
     * @param map     the parameters
     * @param charset charset used to encode params to query string
     */
    public final T params(Map<String, String> map, Charset charset) {
        this.params = convertToList(map.entrySet(), e -> Param.of(e.getKey(), e.getValue()));
        this.paramCharset = charset;
        return self();
    }

    /**
     * Set request body.
     * To get ordinary bodies, see {@link net.dongliu.xhttp.body.Bodies}
     */
    public T body(Body<?> body) {
        this.body = requireNonNull(body);
        return self();
    }

    /**
     * Auto set Accept-Encoding header for request, and if server return compressed content, decompress it. Default is true.
     * this setting will override setting in HTTPClientBuilder.
     */
    public T autoGzip(boolean autoGzip) {
        this.autoGzip = autoGzip;
        return self();
    }

    /**
     * Set http Basic Authentication
     */
    public T basicAuth(String user, char[] password) {
        return basicAuth(new PasswordAuthentication(requireNonNull(user), requireNonNull(password)));
    }

    /**
     * Set http Basic Authentication
     */
    public T basicAuth(PasswordAuthentication basicAuth) {
        this.basicAuth = requireNonNull(basicAuth);
        return self();
    }

    /**
     * Build a immutable request.
     *
     * @return the request.
     */
    public HTTPRequest build() {
        return new HTTPRequest(this);
    }

    protected abstract T self();
}
