package net.dongliu.xhttp;

/**
 * Well-known http header names
 */
public abstract class HTTPHeaderNames {
    public static final String AUTHORIZATION = "Authorization";
    public static final String NAME_PROXY_AUTHORIZATION = "Proxy-Authorization";
    public static final String USER_AGENT = "User-Agent";
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String COOKIE = "Cookie";
    public static final String SET_COOKIE = "Set-Cookie";
    public static final String CONTENT_ENCODING = "Content-Encoding";
    public static final String LOCATION = "Location";
    public static final String REFERER = "Referer";
}
