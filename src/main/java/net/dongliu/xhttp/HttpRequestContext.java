package net.dongliu.xhttp;

import net.dongliu.xhttp.executor.async.AsyncHttpClientRequestExecutor;
import net.dongliu.xhttp.executor.blocking.HttpClientRequestExecutor;

import java.net.URL;

/**
 * For carrying http client for chained call.
 */
public class HttpRequestContext extends AbstractRequestBuilder<HttpRequestContext> {
    private final HTTPClient client;

    HttpRequestContext(HTTPClient client, String method, URL url) {
        super(method, url);
        this.client = client;
    }


    /**
     * Build http request, and send out sync.
     */
    public BlockingResponse send() {
        HTTPRequest request = build();
        var executor = new HttpClientRequestExecutor(client.httpClient, client.interceptors(),
                client.jsonProcessor().orElse(null));
        return executor.proceed(request);
    }

    /**
     * Build http request, and send out sync.
     */
    public AsyncResponse sendAsync() {
        HTTPRequest request = build();
        var asyncExecutor = new AsyncHttpClientRequestExecutor(client.httpClient, client.interceptors(),
                client.jsonProcessor().orElse(null));
        return asyncExecutor.proceedAsync(request);
    }

    @Override
    protected HttpRequestContext self() {
        return this;
    }
}
