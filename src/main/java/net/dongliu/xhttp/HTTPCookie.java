package net.dongliu.xhttp;

import java.time.Instant;
import java.util.Objects;
import java.util.OptionalLong;

import static java.util.Objects.requireNonNull;

/**
 * HTTP cookie, following RFC6265.
 */
public class HTTPCookie implements NameValue {
    private final String name;
    private final String value;

    private final String domain;
    private final String path;
    private final OptionalLong maxAge;
    private final boolean secure;
    private final boolean httpOnly;
    private final Instant createTime;

    protected HTTPCookie(String name, String value, String domain, String path, OptionalLong maxAge,
                         boolean secure, boolean httpOnly, Instant createTime) {
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.path = path;
        this.maxAge = maxAge;
        this.secure = secure;
        this.httpOnly = httpOnly;
        this.createTime = createTime;
    }

    /**
     * This method is for create a particular cookie to set into HTTPRequest Builder.
     * Do not use cookie create by this method for other purposes.
     *
     * @param name  the cookie name
     * @param value the cookie value
     * @return cookie
     */
    public static HTTPCookie of(String name, String value) {
        return new HTTPCookie(requireNonNull(name), requireNonNull(value), "", "", OptionalLong.empty(),
                false, false, Instant.EPOCH);
    }

    /**
     * Cookie domain
     */
    public String domain() {
        return domain;
    }

    /**
     * Cookie path
     */
    public String path() {
        return path;
    }

    public OptionalLong maxAge() {
        return maxAge;
    }

    public boolean secure() {
        return secure;
    }

    public boolean httpOnly() {
        return httpOnly;
    }

    public Instant createTime() {
        return createTime;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HTTPCookie that = (HTTPCookie) o;
        return secure == that.secure &&
                httpOnly == that.httpOnly &&
                Objects.equals(name, that.name) &&
                Objects.equals(value, that.value) &&
                Objects.equals(domain, that.domain) &&
                Objects.equals(path, that.path) &&
                Objects.equals(maxAge, that.maxAge) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, value, domain, path, maxAge, secure, httpOnly, createTime);
    }

    @Override
    public String toString() {
        return "HTTPCookie{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", domain='" + domain + '\'' +
                ", path='" + path + '\'' +
                ", maxAge=" + maxAge +
                ", secure=" + secure +
                ", httpOnly=" + httpOnly +
                ", createTime=" + createTime +
                '}';
    }
}
