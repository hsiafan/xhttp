package net.dongliu.xhttp;

import net.dongliu.commons.annotation.Nullable;
import net.dongliu.xhttp.interceptor.GzipResponseInterceptor;
import net.dongliu.xhttp.json.JsonProcessor;
import net.dongliu.xhttp.utils.Check;
import net.dongliu.xhttp.utils.HonoredCookieManager;

import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.KeyStore;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

import static java.util.Objects.requireNonNull;

/**
 * Builder to build Http Client.
 */
public class HTTPClientBuilder {

    @Nullable ProxySelector proxySelector = null;
    boolean useHttp2 = true;
    boolean verifyCert = true;
    boolean followRedirect = true;
    @Nullable Authenticator authenticator = null;
    CookieHandler cookieHandler = new HonoredCookieManager();
    @Nullable Executor executor = null;
    @Nullable KeyStore keyStore = null;
    List<InterceptorSupplier> interceptorSuppliers = new ArrayList<>(List.of(GzipResponseInterceptor::new));
    // connect timeout, include ssl handshake etc.
    Duration connectTimeout = Duration.ofSeconds(5);

    // The response timeout. this filed can be override by per request setting
    Duration timeout = Duration.ofSeconds(10);
    @Nullable String userAgent = null;
    boolean autoGzip = true;
    @Nullable JsonProcessor jsonProcessor;

    /**
     * Build a new http Client.
     *
     * @return the http client
     */
    public HTTPClient build() {
        return new HTTPClient(this);
    }

    /**
     * Set a proxy selector for this client. If not set, will use no proxy.
     * Note: seems JDK HttpClient Only support http proxy.
     */
    public HTTPClientBuilder proxy(ProxySelector proxySelector) {
        this.proxySelector = requireNonNull(proxySelector);
        return this;
    }

    /**
     * Set a proxy for this client. If not set, will use no proxy.
     * Note: seems JDK HttpClient Only support http proxy.
     */
    public HTTPClientBuilder proxy(Proxy proxy) {
        requireNonNull(proxy);
        this.proxySelector = requireNonNull(ProxySelectors.staticSelector(proxy));
        return this;
    }

    /**
     * If use http2. default true
     */
    public HTTPClientBuilder useHttp2(boolean useHttp2) {
        this.useHttp2 = useHttp2;
        return this;
    }

    /**
     * If check https certificate, default true.
     */
    public HTTPClientBuilder verifyCert(boolean verify) {
        this.verifyCert = verify;
        return this;
    }

    /**
     * If follow redirects. Default true
     */
    public HTTPClientBuilder followRedirect(boolean followRedirect) {
        this.followRedirect = followRedirect;
        return this;
    }

    /**
     * Set timeout for connecting phase. The timeout include ssl handshake, etc. Default is 5 secs.
     */
    public HTTPClientBuilder connectTimeout(Duration timeout) {
        this.connectTimeout = Check.timeout(timeout);
        return this;
    }

    /**
     * The timeout for one request to return response, do not include connect time. Default is 10 secs.
     * Note: The can be override by request setting.
     */
    public HTTPClientBuilder timeout(Duration timeout) {
        this.timeout = Check.timeout(timeout);
        return this;
    }

    /**
     * The client user-agent.
     * Note: The can be override by request setting.
     */
    public HTTPClientBuilder userAgent(String userAgent) {
        this.userAgent = requireNonNull(userAgent);
        return this;
    }

    /**
     * Auto set Accept-Encoding header for request, and if server return compressed content, decompress it. Default is true.
     * Note: This setting can be override by per request setting.
     */
    public HTTPClientBuilder autoGzip(boolean autoGzip) {
        this.autoGzip = autoGzip;
        return this;
    }

    /**
     * Provide Authenticator for http basic or digest Authentication. If not set, will use system default Authenticator.
     *
     * @param authenticator the authenticator
     * @return self
     */
    public HTTPClientBuilder authenticator(Authenticator authenticator) {
        this.authenticator = requireNonNull(authenticator);
        return this;
    }

    /**
     * Provide CookieHandler for handle cookies.If not set, will use default in-memory store CookieManager.
     *
     * @param cookieHandler the cookieManager
     * @return self
     */
    public HTTPClientBuilder cookieHandler(CookieHandler cookieHandler) {
        this.cookieHandler = requireNonNull(cookieHandler);
        return this;
    }

    /**
     * Provide KeyStore for create ssl connection, trust the server certificate.
     *
     * @param keyStore the keyStore
     * @return self
     */
    public HTTPClientBuilder keyStore(KeyStore keyStore) {
        this.keyStore = requireNonNull(keyStore);
        return this;
    }

    /**
     * Set a custom executor, for asynchronous and dependent tasks.
     *
     * @param executor the executor
     * @return self
     */
    public HTTPClientBuilder executor(Executor executor) {
        this.executor = requireNonNull(executor);
        return this;
    }

    /**
     * Set JsonProcessor for this HttpClient, so it can handle json request and response body.
     *
     * @param jsonProcessor cannot be null
     */
    public HTTPClientBuilder jsonProcessor(JsonProcessor jsonProcessor) {
        this.jsonProcessor = requireNonNull(jsonProcessor);
        return this;
    }

    /**
     * Add interceptors
     *
     * @param interceptorSuppliers the interceptor factories
     * @return self
     */
    public HTTPClientBuilder addInterceptors(InterceptorSupplier... interceptorSuppliers) {
        Collections.addAll(this.interceptorSuppliers, interceptorSuppliers);
        return this;
    }

    /**
     * Add interceptors
     *
     * @param interceptorSuppliers the interceptor factories
     * @return self
     */
    public HTTPClientBuilder addInterceptors(List<? extends InterceptorSupplier> interceptorSuppliers) {
        this.interceptorSuppliers.addAll(interceptorSuppliers);
        return this;
    }
}
