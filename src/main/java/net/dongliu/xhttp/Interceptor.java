package net.dongliu.xhttp;

/**
 * Http request Interceptor, for both blocking and async request.
 *
 * @author Liu Dong
 */
public interface Interceptor {

    /**
     * filter HTTPRequest.
     *
     * @param request the request
     * @return new request manifested by this filter
     */
    HTTPRequest onRequest(HTTPRequest request);

    /**
     * Filter blocking response.
     *
     * @param response the blocking response
     * @return new response manifested by this filter
     */
    BlockingResponse.Info onBlockingResponse(BlockingResponse.Info response);

    /**
     * Filter async response.
     *
     * @param response the blocking response
     * @return new response manifested by this filter
     */
    AsyncResponse.Info onAsyncResponse(AsyncResponse.Info response);
}
