module net.dongliu.xhttp {
    requires java.logging;
    requires java.net.http;
    requires net.dongliu.commons;
    // jackson
    requires static com.fasterxml.jackson.core;
    requires static com.fasterxml.jackson.databind;
    // jsonb
    requires static java.json.bind;
    requires static java.json;

    exports net.dongliu.xhttp;
    exports net.dongliu.xhttp.body;
    exports net.dongliu.xhttp.json;
    exports net.dongliu.xhttp.exception;
}