package net.dongliu.xhttp;

import net.dongliu.xhttp.body.Bodies;
import net.dongliu.xhttp.json.JacksonJsonProcessor;
import net.dongliu.xhttp.json.TypeToken;
import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class AsyncClientTest {
    private static MockServer server = new MockServer();

    @BeforeAll
    public static void init() {
        server.start();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    public void testGet() {
        var response = HTTPClient.defaultClient().get("http://127.0.0.1:8080").sendAsync()
                .toTextResponse()
                .join();

        assertEquals(200, response.statusCode());
        assertTrue(response.body().contains("Accept-Encoding=gzip, deflate"));
    }

    @Test
    public void receiveJson() {
        var list = HTTPClient.builder().jsonProcessor(new JacksonJsonProcessor()).build()
                .post("http://127.0.0.1:8080/echo_body")
                .body(Bodies.json(List.of(1, 2, 3)))
                .sendAsync()
                .decodeJson(new TypeToken<List<Integer>>() {})
                .join().body();
        assertEquals(List.of(1, 2, 3), list);
    }
}
