package net.dongliu.xhttp;


import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Liu Dong
 */
public class HTTPHeadersTest {

    private HTTPHeaders headers = HTTPHeaders.of(List.of(
            HTTPHeader.of("Location", "www"),
            HTTPHeader.of("Location", "www2"),
            HTTPHeader.of("Content-Length", "100")
    ));

    @Test
    public void getHeaders() {
        assertEquals(Arrays.asList("www", "www2"), headers.getHeaders("Location"));
    }

    @Test
    public void getHeader() {
        assertEquals(Optional.of("www"), headers.getHeader("Location"));
        assertEquals(Optional.of("www"), headers.getHeader("location"));
    }

    @Test
    public void getLongHeader() {
        assertEquals(OptionalLong.of(100), headers.getLongHeader("Content-Length"));
    }

    @Test
    public void getCharset() {
        assertEquals(Optional.empty(), headers.contentType());
        var headers = HTTPHeaders.of(List.of(
                HTTPHeader.of("Content-Length", "100"),
                HTTPHeader.of("Content-Type", "plain/text; charset=utf-8")
        ));
        assertEquals(Optional.of(UTF_8), headers.contentType().flatMap(ContentType::charset));
    }

    @Test
    public void cookies() {
    }

    @Test
    public void of() {
        var header = HTTPHeader.ofDate("date", ZonedDateTime.of(1984, 1, 2, 10, 10, 10, 0, ZoneId.of("Asia/Shanghai")));
        assertEquals("Mon, 2 Jan 1984 02:10:10 GMT", header.value());

        header = HTTPHeader.ofDate("date", OffsetDateTime.of(1984, 1, 2, 10, 10, 10, 0, ZoneOffset.ofHours(8)));
        assertEquals("Mon, 2 Jan 1984 02:10:10 GMT", header.value());


        Date date = new Date(1525923755682L);
        header = HTTPHeader.ofDate("", date);
        assertEquals("Thu, 10 May 2018 03:42:35 GMT", header.value());
    }

    @Test
    public void nameEquals() {
    }
}