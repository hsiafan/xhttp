package net.dongliu.xhttp;

import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Liu Dong
 */
// TODO: start a local proxy here
@Disabled("need proxy to test")
public class HTTPClientProxyTest {

    private static MockServer server = new MockServer();

    @BeforeAll
    public static void init() {
        server.start();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    public void testHttpProxy() throws Exception {
        // http proxy with redirect
        var response = HTTPClient.builder()
                .proxy(Proxies.httpProxy("127.0.0.1", 2080))
                .build()
                .get("http://127.0.0.1:8080")
                .send().discard();
        assertEquals(200, response.statusCode());
    }

    @Test
    public void testSocksProxy() throws Exception {
        // socks proxy with redirect
        var response = HTTPClient.builder()
                .proxy(Proxies.socksProxy("127.0.0.1", 2080))
                .build()
                .get("http://127.0.0.1:8080/echo_header")
                .send().discard();
        assertEquals(200, response.statusCode());
    }
}
