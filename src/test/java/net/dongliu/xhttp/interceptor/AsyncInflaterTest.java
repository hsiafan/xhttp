package net.dongliu.xhttp.interceptor;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.zip.GZIPOutputStream;

import static net.dongliu.xhttp.interceptor.AsyncInflater.GZIP;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class AsyncInflaterTest {

    @Test
    public void decode() throws IOException {
        byte[] data = new byte[50000];
        var random = new Random();
        random.nextBytes(data);

        var out = new ByteArrayOutputStream();
        var gout = new GZIPOutputStream(out);
        gout.write(data);
        gout.flush();
        gout.finish();
        byte[] compressed = out.toByteArray();

        var bos = new ByteArrayOutputStream();
        var inflater = new AsyncInflater(GZIP);
        byte[] buf = new byte[1024];
        int offset = 0;
        while (offset < compressed.length) {
            int toCopy = Math.min(compressed.length - offset, buf.length);
            System.arraycopy(compressed, offset, buf, 0, toCopy);
            offset += toCopy;
            inflater.decode(ByteBuffer.wrap(buf), buffer -> bos.write(buffer.array(), 0, buffer.limit()));
        }
        inflater.finished();

        var decompressed = bos.toByteArray();
        assertArrayEquals(data, decompressed);

    }
}