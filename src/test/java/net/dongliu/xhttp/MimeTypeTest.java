package net.dongliu.xhttp;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MimeTypeTest {

    @Test
    public void testToString() {
        var html = MimeType.of("text", "html");
        assertEquals("text/html", html.toString());
    }

    @Test
    public void parse() {
        assertEquals(MimeType.HTML, MimeType.safeParse("text/html").orElseThrow());
    }
}