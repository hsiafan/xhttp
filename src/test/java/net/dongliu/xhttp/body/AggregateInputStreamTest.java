package net.dongliu.xhttp.body;

import net.dongliu.commons.io.InputStreams;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


public class AggregateInputStreamTest {

    @Test
    public void read() throws IOException {
        ByteArrayInputStream in1 = new ByteArrayInputStream("hello ".getBytes());
        ByteArrayInputStream in2 = new ByteArrayInputStream("world!".getBytes());
        AggregateInputStream in = new AggregateInputStream(in1, in2);
        assertArrayEquals("hello world!".getBytes(), InputStreams.readAll(in));
        in.close();
    }
}