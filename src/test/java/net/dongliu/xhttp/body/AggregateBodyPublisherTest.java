package net.dongliu.xhttp.body;

import org.junit.jupiter.api.Test;

import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodySubscribers;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.Flow;
import java.util.concurrent.Flow.Subscriber;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AggregateBodyPublisherTest {

    @Test
    void test() {
        var publisher1 = BodyPublishers.ofByteArray("this is a test.".getBytes());
        var publisher2 = BodyPublishers.ofByteArray("that is a test.".getBytes());
        var publisher = new AggregateBodyPublisher(List.of(publisher1, publisher2));
        var bodySubscriber = BodySubscribers.ofString(UTF_8);
        publisher.subscribe(new BodySubscriberAdapter(bodySubscriber));

        String body = bodySubscriber.getBody().toCompletableFuture().join();
        assertEquals("this is a test.that is a test.", body);
    }

    @Test
    void test0() {
        var publisher1 = BodyPublishers.ofByteArray("this is a test.".getBytes());
        var publisher = new AggregateBodyPublisher(List.of(publisher1));
        var bodySubscriber = BodySubscribers.ofString(UTF_8);
        publisher.subscribe(new BodySubscriberAdapter(bodySubscriber));

        String body = bodySubscriber.getBody().toCompletableFuture().join();
        assertEquals("this is a test.", body);
    }

    private static class BodySubscriberAdapter implements Subscriber<ByteBuffer> {
        private final HttpResponse.BodySubscriber<String> bodySubscriber;

        public BodySubscriberAdapter(HttpResponse.BodySubscriber<String> bodySubscriber) {
            this.bodySubscriber = bodySubscriber;
        }

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            bodySubscriber.onSubscribe(subscription);
        }

        @Override
        public void onNext(ByteBuffer item) {
            bodySubscriber.onNext(List.of(item));
        }

        @Override
        public void onError(Throwable throwable) {
            bodySubscriber.onError(throwable);
        }

        @Override
        public void onComplete() {
            bodySubscriber.onComplete();
        }
    }
}