//package net.dongliu.http.body;
//
//import net.dongliu.http.json.JsonLookup;
//import org.junit.Test;
//
//import java.io.ByteArrayOutputStream;
//import java.nio.ByteBuffer;
//import java.util.Arrays;
//import java.util.concurrent.Flow;
//
//import static java.nio.charset.StandardCharsets.UTF_8;
//import static org.junit.Assert.assertEquals;
//
//public class RequestBodyTest {
//    @Test
//    public void json() throws Exception {
//        var body = Body.json(Arrays.asList("1", "2", "3"));
//        assertEquals("application/json", body.contentType());
//
//        JsonBody.class.cast(body).jsonProcessor(JsonLookup.getInstance().lookup().orElse(null));
//
//        var publisher = body.asBodyPublisher(UTF_8);
//        var subscriber = new Flow.Subscriber<ByteBuffer>() {
//            private Flow.Subscription subscription;
//            private ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//            @Override
//            public void onSubscribe(Flow.Subscription subscription) {
//                this.subscription = subscription;
//                subscription.request(1);
//            }
//
//            @Override
//            public void onNext(ByteBuffer item) {
//                bos.write(item.array(), item.arrayOffset() + item.position(), item.remaining());
//                subscription.request(1);
//            }
//
//            @Override
//            public void onError(Throwable throwable) {
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        };
//        publisher.subscribe(subscriber);
//        var str = subscriber.bos.toString(UTF_8);
//        assertEquals("[\"1\",\"2\",\"3\"]", str);
//    }
//
//}