package net.dongliu.xhttp;

import net.dongliu.xhttp.utils.URIEncoder;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class URIEncoderTest {
    @Test
    public void joinUrl() throws Exception {
        assertEquals("http://www.test.com/", URIEncoder.joinUrl(new URL("http://www.test.com/"),
                List.of(), StandardCharsets.UTF_8).toExternalForm());
        assertEquals("http://www.test.com/path", URIEncoder.joinUrl(new URL("http://www.test.com/path"),
                List.of(), StandardCharsets.UTF_8).toExternalForm());

        assertEquals("http://www.test.com/path?t=v", URIEncoder.joinUrl(new URL("http://www.test.com/path"),
                List.of(Param.of("t", "v")), StandardCharsets.UTF_8).toExternalForm());
        assertEquals("http://www.test.com/path?s=t&t=v", URIEncoder.joinUrl(new URL("http://www.test.com/path?s=t"),
                List.of(Param.of("t", "v")), StandardCharsets.UTF_8).toExternalForm());
        assertEquals("http://www.test.com/path?t=v", URIEncoder.joinUrl(new URL("http://www.test.com/path?"),
                List.of(Param.of("t", "v")), StandardCharsets.UTF_8).toExternalForm());
        assertEquals("http://www.test.com/path?t=v#seg", URIEncoder.joinUrl(new URL("http://www.test.com/path#seg"),
                List.of(Param.of("t", "v")), StandardCharsets.UTF_8).toExternalForm());
        assertEquals("http://www.test.com/path?t=v#", URIEncoder.joinUrl(new URL("http://www.test.com/path#"),
                List.of(Param.of("t", "v")), StandardCharsets.UTF_8).toExternalForm());
    }

}