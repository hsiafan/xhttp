package net.dongliu.xhttp;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled("deepened on outer resources, manual run this test")
public class OuterTest {

    @Test
    void test() {
        var resp = HTTPClient.defaultClient().get("http://www.baidu.com").send().toTextResponse();
        assertEquals(200, resp.statusCode());
        assertEquals("gzip", resp.headers().getHeader("Content-Encoding").orElseThrow());
        assertTrue(resp.body().contains("html"));
    }

    @Test
    void testAsync() {
        var resp = HTTPClient.defaultClient().get("http://www.baidu.com").sendAsync().toTextResponse().join();
        assertEquals(200, resp.statusCode());
        assertEquals("gzip", resp.headers().getHeader("Content-Encoding").orElseThrow());
        assertTrue(resp.body().contains("html"));
    }
}
