package net.dongliu.xhttp.mock;


import net.dongliu.commons.io.Readers;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Liu Dong {@literal <dongliu@live.cn>}
 */
public class MockEchoBodyServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String body = Readers.readAll(request.getReader());
        request.getReader().close();

        response.setContentType("text/plain");
        response.setCharacterEncoding(request.getCharacterEncoding());
        PrintWriter out = response.getWriter();
        out.write(body);
        out.flush();
    }
}
