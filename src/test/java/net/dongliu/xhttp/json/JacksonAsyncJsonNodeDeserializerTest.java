package net.dongliu.xhttp.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class JacksonAsyncJsonNodeDeserializerTest {

    @Test
    public void parse() throws IOException {
        var mapper = new ObjectMapper();
        var deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("{}".getBytes()));
        var node = deserializer.complete();
        assertEquals(JsonNodeType.OBJECT, node.getNodeType());
        assertEquals(0, node.size());

        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("[]".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.ARRAY, node.getNodeType());
        assertEquals(0, node.size());

        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("1".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.NUMBER, node.getNodeType());
        assertEquals(1, node.asInt());

        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("\"1\"".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.STRING, node.getNodeType());
        assertEquals("1", node.asText());

        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("true".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.BOOLEAN, node.getNodeType());
        assertTrue(node.asBoolean());

        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("null".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.NULL, node.getNodeType());

        // complicated
        deserializer = new JacksonAsyncJsonNodeDeserializer(mapper);
        deserializer.feed(ByteBuffer.wrap("{\"numbers\":[1,2,3], \"values\": {\"name\":\"jerry\"}}".getBytes()));
        node = deserializer.complete();
        assertEquals(JsonNodeType.OBJECT, node.getNodeType());
        assertEquals(JsonNodeType.ARRAY, node.get("numbers").getNodeType());
        assertEquals(1, node.get("numbers").get(0).asInt());
        assertEquals(2, node.size());
    }
}