package net.dongliu.xhttp.json;

import net.dongliu.commons.io.InputStreams;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ByteBuffersInputStreamTest {

    @Test
    public void read() throws IOException {
        Random random = new Random();
        byte[] bytes = new byte[10000];
        random.nextBytes(bytes);
        InputStream in = mockInput(bytes);
        assertArrayEquals(bytes, InputStreams.readAll(in));
    }


    @Test
    public void readAllBytes() throws IOException {
        Random random = new Random();
        byte[] bytes = new byte[10000];
        random.nextBytes(bytes);
        InputStream in = mockInput(bytes);
        assertArrayEquals(bytes, in.readAllBytes());
    }

    private ByteBuffersInputStream mockInput(byte[] bytes) {
        var buffers = new ArrayList<ByteBuffer>();
        for (int i = 0; i < (bytes.length - 1) / 1000 + 1; i++) {
            buffers.add(ByteBuffer.wrap(bytes, i * 1000, Math.min(1000, bytes.length - i * 1000)));
        }
        buffers.add(ByteBuffer.allocate(0));
        return new ByteBuffersInputStream(buffers);
    }
}