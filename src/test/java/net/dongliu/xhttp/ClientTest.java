package net.dongliu.xhttp;

import net.dongliu.xhttp.body.Bodies;
import net.dongliu.xhttp.body.Parts;
import net.dongliu.xhttp.json.JacksonJsonProcessor;
import net.dongliu.xhttp.json.TypeToken;
import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class ClientTest {
    private static MockServer server = new MockServer();
    private static HTTPClient client;

    @BeforeAll
    public static void init() {
        server.start();
        client = HTTPClient.defaultClient();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    public void testGet() {
        String resp = client.get("http://127.0.0.1:8080")
                .send().toTextResponse().body();
        assertFalse(resp.isEmpty());

        resp = client.get("http://127.0.0.1:8080").send().toTextResponse().body();
        assertFalse(resp.isEmpty());

        // get with params
        Map<String, String> map = new HashMap<>();
        map.put("wd", "test");
        resp = client.get("http://127.0.0.1:8080").params(map).send().toTextResponse().body();
        assertFalse(resp.isEmpty());
        assertTrue(resp.contains("wd=test"));
    }

    @Test
    public void testHead() {
        var resp = client.head("http://127.0.0.1:8080").send().toTextResponse();
        assertEquals(200, resp.statusCode());
        String text = resp.body();
        assertTrue(text.isEmpty());
    }

    @Test
    public void testPost() {
        // form encoded post
        String text = client.post("http://127.0.0.1:8080/post")
                .body(Bodies.wwwForm(Param.of("wd", "test")))
                .send().toTextResponse().body();
        assertTrue(text.contains("wd=test"));
    }

    @Test
    public void testCookie() {
        var response = client.get("http://127.0.0.1:8080/cookie")
                .cookies(HTTPCookie.of("test", "value"))
                .send().toTextResponse();
        boolean flag = false;
        for (var cookie : response.cookies()) {
            if (cookie.nameEquals("test")) {
                flag = true;
                break;
            }
        }
        assertTrue(flag);
    }


    @Test
    public void testRedirect() {
        var resp = client.get("http://127.0.0.1:8080/redirect").send().toTextResponse();
        assertEquals(200, resp.statusCode());
        String body = resp.body();
        assertTrue(body.contains("/redirected"));
    }

    @Test
    public void testMultiPart() {
        String body = client.post("http://127.0.0.1:8080/multi_part")
                .body(Bodies.multiPart(
                        Parts.file("file_field1", "keystore", () -> this.getClass().getResourceAsStream("/keystore"), MimeType.OCTET_STREAM)
                )).send().toTextResponse().body();
        assertTrue(body.contains("file_field1"));
        assertTrue(body.contains("application/octet-stream"));
    }


    @Test
    public void testMultiPartText() {
        String body = client.post("http://127.0.0.1:8080/multi_part")
                .body(Bodies.multiPart(Parts.text("test", "this is test value")))
                .send().toTextResponse().body();
        assertTrue(body.contains("this is test value"));
        assertTrue(!body.contains("Content-Type"));
    }

    @Test
    public void sendJson() {
        String text = HTTPClient.builder().jsonProcessor(new JacksonJsonProcessor()).build()
                .post("http://127.0.0.1:8080/echo_body")
                .body(Bodies.json(List.of(1, 2, 3)))
                .send().toTextResponse().body();
        assertTrue(text.startsWith("["));
        assertTrue(text.endsWith("]"));
    }

    @Test
    public void receiveJson() {
        var list = HTTPClient.builder().jsonProcessor(new JacksonJsonProcessor()).build()
                .post("http://127.0.0.1:8080/echo_body")
                .body(Bodies.json(List.of(1, 2, 3)))
                .send().decodeJson(new TypeToken<List<Integer>>() {}).body();
        assertEquals(List.of(1, 2, 3), list);
    }

    @Test
    public void sendHeaders() {
        String text = client.get("http://127.0.0.1:8080/echo_header")
                .headers(HTTPHeader.ofLong("TestHeader", 1))
                .send().toTextResponse().body();
        assertTrue(text.contains("TestHeader: 1"));
    }

    @Test
    public void compress() {
        String text = client.get("http://127.0.0.1:8080/echo_header")
                .autoGzip(true)
                .send().toTextResponse().body();
        assertTrue(text.contains("Accept-Encoding: gzip, deflate"));
    }


    @Test
    public void testBasicAuth() {
        var response = client.get("http://127.0.0.1:8080/basicAuth")
                .basicAuth("test", "password".toCharArray())
                .send().toTextResponse();
        assertEquals(200, response.statusCode());
    }

    @Test
    public void testHttps() {
        var response = HTTPClient.builder().verifyCert(false).build()
                .get("https://127.0.0.1:8443/https")
                .send().toTextResponse();
        assertEquals(200, response.statusCode());


        response = HTTPClient.builder()
                .keyStore(KeyStores.load(getClass().getResourceAsStream("/keystore"), "123456".toCharArray()))
                .build()
                .get("https://127.0.0.1:8443/https")
                .send().toTextResponse();
        assertEquals(200, response.statusCode());
    }

}