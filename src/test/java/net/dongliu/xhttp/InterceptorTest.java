package net.dongliu.xhttp;

import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled("Need to improve interceptor")
public class InterceptorTest {

    private static MockServer server = new MockServer();

    @BeforeAll
    public static void init() {
        server.start();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    public void testInterceptor() {
//        var client = HttpClient.defaultClient();
//        final long[] statusCode = {0};
//        Interceptor interceptor = (target, request) -> {
//            BlockingResponse response = target.proceed(request);
//            statusCode[0] = response.statusCode();
//            return response;
//        };
//
//        String text = client.get("http://127.0.0.1:8080/echo_header")
//                .interceptors(interceptor)
//                .send().toTextResponse().body();
//        assertFalse(text.isEmpty());
//        assertTrue(statusCode[0] > 0);
    }
}
