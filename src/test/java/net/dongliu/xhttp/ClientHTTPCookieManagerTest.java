package net.dongliu.xhttp;

import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class ClientHTTPCookieManagerTest {

    private static MockServer server = new MockServer();

    @BeforeAll
    public static void init() {
        server.start();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    public void testSession() {
        var client = HTTPClient.defaultClient();
        var response = client.get("http://127.0.0.1:8080/cookie")
                .cookies(HTTPCookie.of("test", "value"))
                .send().toTextResponse();
        boolean flag = false;
        for (var cookie : response.cookies()) {
            if (cookie.nameEquals("test")) {
                flag = true;
                break;
            }
        }
        assertTrue(flag);

        client.get("http://127.0.0.1:8080/set_cookie").send().toTextResponse();
        String text = client.get("http://127.0.0.1:8080/echo_header")
                .cookies(HTTPCookie.of("test_cookie", "test_cookie_value"))
                .send().toTextResponse().body();
        assertTrue(text.contains("server_cookie=server_cookie_value"));
        assertTrue(text.contains("test_cookie=test_cookie_value"));
    }
}