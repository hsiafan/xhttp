package net.dongliu.xhttp;

import net.dongliu.xhttp.mock.MockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClientTestSysHeaders {

    private static MockServer server = new MockServer();

    @BeforeAll
    public static void init() {
        server.start();
    }

    @AfterAll
    public static void destroy() {
        server.stop();
    }

    @Test
    void testUserAgent() {
        var client = HTTPClient.defaultClient();
        var resp = client.get("http://127.0.0.1:8080")
                .userAgent("my-user-agent")
                .send().toTextResponse();
        assertEquals(200, resp.statusCode());
        String body = resp.body();
        assertTrue(body.contains("User-Agent=my-user-agent"));
    }

    @Test
    @Disabled("Cannot set Referer/Origin headers. see #JDK-8203850")
    void setReferer() {
        var client = HTTPClient.defaultClient();
        var resp = client.get("http://127.0.0.1:8080")
                .referer("my-referer")
                .send().toTextResponse();
        assertEquals(200, resp.statusCode());
        String body = resp.body();
        assertTrue(body.contains("Referer=my-referer"));
    }

    @Test
    @Disabled("HttpClient does not allow set private headers(Host, Date, expect, from, via, upgrade, etc)")
    void sendPrivateHeaders() {
        var client = HTTPClient.defaultClient();
        String text = client.get("http://127.0.0.1:8080/echo_header")
                .headers(HTTPHeader.of("Host", "www.test.com"))
                .send().toTextResponse().body();
        assertTrue(text.contains("Host: www.test.com"));
    }
}
