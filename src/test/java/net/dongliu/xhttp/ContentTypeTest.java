package net.dongliu.xhttp;


import org.junit.jupiter.api.Test;

import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContentTypeTest {

    @Test
    public void testToString() {
        var contentType = ContentType.of(MimeType.HTML, UTF_8);
        assertEquals("text/html; charset=utf-8", contentType.toString());
        var multiPart = ContentType.multiPart("=======******=====");
        assertEquals("multipart/form-data; boundary========******=====", multiPart.toString());
    }

    @Test
    public void parse() {
        var htmlType = ContentType.safeParse("text/html; charset=utf-8").orElseThrow();
        assertEquals(MimeType.HTML, htmlType.mimeType());
        assertEquals(Optional.of(UTF_8), htmlType.charset());
    }
}