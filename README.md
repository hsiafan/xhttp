A convenient Http lib based on JDK HttpClient, support HTTP2 and async requests.

Add dependency:

```xml
<dependency>
    <groupId>net.dongliu</groupId>
    <artifactId>xhttp</artifactId>
    <version>1.1.0</version>
</dependency>
```

Features:

* www-form-encode posting
* MultiPart posting
* Json http body handle using Jackson or Jsonb
* Gzip response handle
* Disable SSL certificate check, or use specified certificate.


For now(JDK 11)，The HttpClient:
* Cannot explicit close the HTTP client, or it's connection pool.
* Cannot set concurrent connection nums for each site or in total, which is necessary for Http1.x.
* Cannot set Host/Date/... headers. 
* Cannot set Referer/Origin headers #JDK-8203850 
* <strike>Cannot override default user-agent.   #JDK-8203771</strike>
* Cannot create connection with a socks proxy.
* Cannot disable Hostname Verification(Only can set by env variable for whole java process).
